IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2085708/raw/master/restore-k8s.sh && chmod +x restore-k8s.sh && source restore-k8s.sh kubernetes-training-step2
</pre>

**1. Declarative vs Imperative configuration management** 

<i>Note:</i> We will be using `bookinfo-practice` folder as the root folder for all the commands in this exercise, so please navigate to this directory.

We've been using the `kubectl create` and `kubectl delete` commands until now to create our pods. There is also a `kubectl replace` command. This is the "imperative approach" to Kubernetes, where we have to specify the operation and manage the objects ourselves every time we want to change something.  
  
The Kubernetes recommended approach is the "declarative way", where we supply kubernetes with one or more yaml files. They will represent the **desired state** of our cluster. Based on this, Kubernetes will try to create, modify and delete objects in order to reach this state. For this we will use the `kubectl apply` command to pass the **desired state** to Kubernetes. 
For example, if we pass it the yaml file for our ReplicaSet, it will see that such an object does not exist yet so it has to create it.  
If we modify the yaml file and pass it again, this time it sees that already has the object so it must update it, it will compare, field by field, the new and the old configuration and will update only the fields that changed.      
Also, we can pass multiple files to it by passing a folder. In this case Kubernetes will create all the objects for which it can find a configuration file.  If we remove one configuration file from the folder and pass it again to our cluster, it detects the missing configuration file and will delete the object.
More about Kubernetes declarative and imperative styles of configuration management can be found [here](https://kubernetes.io/docs/concepts/overview/working-with-objects/object-management/), [here](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/) and [here](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/imperative-config/)  
   
**2. Creating the yaml file**  
We will create a file called 'bookinfo-recommendations-replicaset.yaml' in the `bookinfo-practice` folder.
Here is our `ReplicaSet` definition:
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: bookinfo-recommendations
  labels:
    app: bookinfo
    tier: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      service: recommendations
  template:
    metadata:
      labels:
        service: recommendations
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:1.0
        env:
          - name: LIBRARIAN_NAME
            value: John
</pre>

We specify we want a `ReplicatSet` object and we also specify the same metadata as we did with our pod since this is the object we will be interacting with from now on, this is our app now.   
We see we have to specify more information for the spec object:  
  * **`replicas`** - the number of pod/instances we want of our app
  * **`selector`** - the ReplicaSet will use this selector to identify the pods it  needs to control(scale out, scale down, restart etc). This is based on the labels. 
  * **`template`** - is the template for the pods that will be run by the ReplicaSet and it's basically a pod definition without the 'apiVersion' and 'kind' fields. We also don't have to specify a name, Kubernetes will generate the name of each pod from the 'ReplicaSet' name. The label is very important here because it has to match the `selector`
  
**3. Deploy the replicaset**    
Let's deploy our `ReplicaSet` using the declarative way. 
<pre>
kubectl apply -f bookinfo-recommendations-replicaset.yaml
</pre>

If we look now at our cluster we see we have 2 pods deployed to the cluster, that means our ReplicaSet was succesfully created and it already did it's job. 
<pre>
$ kubectl get pods
NAME                             READY   STATUS    RESTARTS   AGE
bookinfo-recommendations-249q9   1/1     Running   0          6s
bookinfo-recommendations-8dq8c   1/1     Running   0          6s
</pre>

**4. A closer look to the replicaset**  
Let's look closer at one of the pods first(the pod name might be different for you):
<pre>
kubectl describe pod bookinfo-recommendations-249q9
</pre>

<pre>
...
IPs:
  IP:           172.18.0.5
<b>Controlled By:  ReplicaSet/bookinfo-recommendations</b>
Containers:
  bookinfo-recommendations:
...</pre>
We see a new field here `Controlled By:  ReplicaSet/bookinfo-recommendations`. This means this pod is managed by a replicaset. Let's take a closer look at that object now. 
<pre>
#any of this four commands will work the same
$  <b>kubectl describe rs bookinfo-recommendations</b> 
# kubectl describe replicaset bookinfo-recommendations
# kubectl describe rs/bookinfo-recommendations
# kubectl describe replicaset/bookinfo-recommendations
</pre>

<pre>
Name:         bookinfo-recommendations
Namespace:    default
Selector:     service=recommendations
Labels:       app=bookinfo
              tier=backend
Annotations:  kubectl.kubernetes.io/last-applied-configuration: ➊
                {"apiVersion":"apps/v1","kind":"ReplicaSet","metadata":{"annotations":{},"labels":{"app":"bookinfo","tier":"backend"},"name":"bookinfo-recommendations"}
Replicas:     2 current / 2 desired  ➋
Pods Status:  2 Running / 0 Waiting / 0 Succeeded / 0 Failed 
Pod Template:  ➌
  Labels:  service=recommendations
  Containers:
   bookinfo-recommendations:
    Image:      cloudacademyibmro/bookinfo-recommendations:1.0
    Port:       <none>
    Host Port:  <none>
    Environment:
      LIBRARIAN_NAME:  John
    Mounts:            <none>
  Volumes:             <none>
Events:                
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  19m   replicaset-controller  Created pod: bookinfo-recommendations-skvb4
  Normal  SuccessfulCreate  19m   replicaset-controller  Created pod: bookinfo-recommendations-jdhhz
</pre>

➊ Kubernetes added a annotation for our object. Annotations are similar to labels, but they are not used to identify the app, they are used just as extra information about the object. In this case Kubernetes added the last applied configuration(yaml file), so next time we do a `kubectl apply` for this object it will compare it with this version to see what must be changed. 

➋ The replicaset will track the number of replicas/instances. If one of the pods goes down, it will bring another one up.  

➌ The replicaset also has the template for our pods  

We can also see all of our replicasets by running: 
<pre>
$ kubectl get rs
NAME                       DESIRED   CURRENT   READY   AGE
bookinfo-recommendations   2         2         2       178m
</pre>

**5. Self healing**  
Now let's expore the self healing capabilities of Kubernetes. Our application has one GET endpoint that once called it will kill the process: `recommendations-app/kill`. We will use this to simulate a crash in our app and see if Kubernetes will restart the container. 

We will do a port-forward from our host to the container inside one of the Kubernetes pods. The pod name might be different for you. 
<pre>
$ kubectl get pods
NAME                             READY   STATUS    RESTARTS   AGE
<b>bookinfo-recommendations-jdhhz</b>   1/1     Running   0          7m8s
bookinfo-recommendations-skvb4   1/1     Running   0          7m8s
$ kubectl port-forward <b>bookinfo-recommendations-jdhhz</b> 3000:3000
</pre>

Open a new terminal and execute `watch kubectl get pods`. This will refresh the information about your pods every second so you can see it in real time. 

Make a GET request at `localhost:3000/recommendations-app/kill` while keeping open the terminal with the `watch command`. You should see that one of the pods will change its status to 'Error' for a few seconds

If you are running on Katacoda, as explained in step1, you have two options for the GET request: 
1. open a new terminal and use curl to see the result
2. you can change the default binding to `0.0.0.0`.  The command is: `kubectl port-forward bookinfo-recommendations 3000:3000 --address 0.0.0.0` . Then you can access from the browser at: https://[[HOST_SUBDOMAIN]]-3000-[[KATACODA_HOST]].environments.katacoda.com/recommendations-app/kill

<pre>
$ watch kubectl get pods
NAME                             <b>READY</b>   <b>STATUS</b>    RESTARTS   AGE
<b>bookinfo-recommendations-jdhhz</b>   <b>0/1</b>     <b>Error</b>     0          7m12s
bookinfo-recommendations-skvb4   1/1     Running   0          7m12s
</pre>

Then the status should change back to running after a few seconds, but look at the 'RESTARTS' column
<pre>
$ watch kubectl get pods
NAME                             READY   STATUS    <b>RESTARTS</b>   AGE
<b>bookinfo-recommendations-jdhhz</b>   1/1     Running   <b>1</b>          7m16s
bookinfo-recommendations-skvb4   1/1     Running   0          7m16s
</pre>

The replicaset noticed that the container inside the pod crashed so it automatically restarted it.
If we delete a pod (we simulate in this way that something was wrong with the whole pod) the replicaset will just create another one. Let's try this too. Keep 2 terminals open in the same time, in one of them run `watch kubectl get pods` so we track the pods in real time. 

<pre>
$ watch kubectl get pods                                                                                            

NAME                             READY   STATUS    RESTARTS   AGE
bookinfo-recommendations-<b>jdhhz</b>   1/1     Running   1          29m
bookinfo-recommendations-<b>skvb4</b>   1/1     Running   0          29m
</pre>

I have 2 containers called **jdhhz** and **skvb4**, you might have different names. I will delete the first one
<pre>
kubectl delete pod bookinfo-recommendations-jdhhz 
</pre>

<pre>
$ watch kubectl get pods

NAME                             READY   STATUS              RESTARTS   AGE
<b>bookinfo-recommendations-6xdsh   0/1     ContainerCreating   0   	1s</b>
bookinfo-recommendations-skvb4   1/1     Running             0   	36m
<b>bookinfo-recommendations-jdhhz   0/1     Terminating         0		36m</b>
</pre>

<pre>
$ watch kubectl get pods

NAMEE                             READY   STATUS    RESTARTS   AGE
<b>bookinfo-recommendations-6xdsh   1/1     Running   0           40s</b>
bookinfo-recommendations-skvb4   1/1     Running   0   	       37m
</pre>

You can see how fast the replicaset start a new pod, it doesn't even wait for the old one to completely exit.   
  
**6. Scale up the application**  
There are multiple ways to achieve this. First let's try by modifying the yaml file and applied it again. We will scale from 2 to 4 instances. 
<pre>
....
spec:
  replicas: <b>4</b>
...
</pre>

<pre>
kubectl apply -f bookinfo-recommendations-replicaset.yaml
</pre>

<pre>
$ kubectl get pods

NAME                             READY   STATUS    RESTARTS   AGE
bookinfo-recommendations-6xdsh   1/1     Running   0   	      32m
bookinfo-recommendations-skvb4   1/1     Running   0	      68m
<b>bookinfo-recommendations-tctl4   1/1     Running   0          16s
bookinfo-recommendations-n2d45   1/1     Running   0          16s</b>
</pre>

The replicaset created 2 more pods automatically. Let's take a look at the events in our replicaset
<pre>
....
Events:
  Type    Reason            Age    From                   Message
  ----    ------            ----   ----                   -------
  Normal  SuccessfulCreate  35m    replicaset-controller  Created pod: bookinfo-recommendations-tqv8r
  Normal  SuccessfulCreate  35m    replicaset-controller  Created pod: bookinfo-recommendations-6xdsh
  <b>Normal  SuccessfulCreate  3m12s  replicaset-controller  Created pod: bookinfo-recommendations-n2d45
  Normal  SuccessfulCreate  3m12s  replicaset-controller  Created pod: bookinfo-recommendations-tctl4</b>
</pre>

We can see when the last 2 pods where created compared to the other two, they were not created at the same moment, so by running `kubectl apply` we updated the replicaset.   
Another way to achieve this will be using this command:
<pre>
kubectl scale --replicas=2 rs bookinfo-recommendations
</pre>

This time we scaled down our application, from 4 instances to just 2 instances
<pre>
$ kubectl get pods
NAME                             READY   STATUS    RESTARTS   AGE
bookinfo-recommendations-6xdsh   1/1     Running   0          35m
bookinfo-recommendations-tctl4   1/1     Running   0          5m12s
</pre>


You can read more about `ReplicaSet` objects [here](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/#how-a-replicaset-works).  

Now Kubernetes takes care of our pods for us, we can start up a bunch of them and we also used some Kubernetes self healing features. But now let's say we want to update our application, deploy version 2.0. How do we update it with 0 downtime? What if we want to have 2 versions running in parallel? And if something goes wrong how can we rollback to the previous version? Kubernetes solution for this is another object called a `Deployment` and we will see how to create one and how to take advantage of it.

We need to clean up the cluster
<pre>
kubectl delete rs bookinfo-recommendations
</pre>
