IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2085708/raw/master/restore-k8s.sh && chmod +x restore-k8s.sh && source restore-k8s.sh kubernetes-training-step7 && kubectl config set-context --current --namespace=bookinfo-ns
 && kubectl apply -f bookinfo-kubernetes/
</pre>

Let's deploy the next component of our app, `ratings-app`. We will run a `postgres` instance in a container in Kubernetes. We will also need to persist the data so in case of a postgres container failure, we don't lose it.

Let's build and push the image to DockherHub. Don't forget to modify the username. 
```
docker build -t bookinfo-ratings:1.0 bookinfo-ratings
docker push bookinfo-ratings:1.0
``` 

Let's start by creating a deployment for our postgres database `bookinfo-ratings-db.yaml`. We will write a deployment and a service in the same file. We split the declaration of the two by using `---`
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  <b>name: bookinfo-ratings-db</b>
  labels:
    app: bookinfo
    <b>tier: db</b>
  namespace: bookinfo-ns
spec:
  replicas: 1
  selector:
    matchLabels:
      <b>service: ratings-db</b>
  template:
    metadata:
      labels:
        <b>service: ratings-db</b>
    spec:
      containers:
      - name: bookinfo-ratings-db
        <b>image: postgres:13</b>
        env:
        - name: POSTGRES_USER
          value: postgres
        - name: POSTGRES_PASSWORD
          value: password
        - name: POSTGRES_DB
          value: ratingsdb

<b>---</b>

apiVersion: v1
kind: Service
metadata:
    name: postgres-ratings
    namespace: bookinfo-ns
spec:
    <b>selector:
        service: ratings-db</b>
    ports:
    <b>- port: 5432</b>
</pre>

Notice that we changed the label `tier` to `db`. We used the `postgres:13` image from Dockerhub. We will use the service name `postgres-ratings` as hostname to access the postgres service. 

Next, let's create the deployment for the spring application `bookinfo-ratings-deployment.yaml`
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  <b>name: bookinfo-ratings</b>
  namespace: bookinfo-ns
  labels:
    app: bookinfo
    tier: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      service: ratings
  template:
    metadata:
      labels:
        service: ratings
    spec:
      containers:
      - name: bookinfo-ratings
        <b>image: cloudacademyibmro/bookinfo-ratings:1.0</b>
        env:
        - name: DB_PORT
          <b>value: "5432"</b>
        - name: DB_HOSTNAME
         <b> value: postgres-ratings</b>
        - name: PORT
          value: "8080"
</pre>

Notice the `DB_HOSTNAME` and `DB_PORT`, they must match the name and port of the `postgres-rating` service. 

We can now deploy it to the Kubernetes cluster and check the logs of the Spring application to see if everything is ok. 
<pre>
<b>$ kubectl apply -f bookinfo-kubernetes</b>
<b>$ kubectl get all</b>
NAME                                       READY   STATUS    RESTARTS   AGE
pod/bookinfo-ratings-589cfdddcf-cbj9w      1/1     Running   0          22m
pod/bookinfo-ratings-db-598868554f-m6vmq   1/1     Running   0          23m

NAME                       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/postgres-ratings   ClusterIP   10.109.229.114   <none>        5432/TCP   24m

NAME                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bookinfo-ratings      1/1     1            1           22m
deployment.apps/bookinfo-ratings-db   1/1     1            1           23m

NAME                                             DESIRED   CURRENT   READY   AGE
replicaset.apps/bookinfo-ratings-589cfdddcf      1         1         1       22m
replicaset.apps/bookinfo-ratings-db-598868554f   1         1         1       23m
<b>$ kubectl logs -f pod/bookinfo-ratings-589cfdddcf-cbj9w</b>
</pre>

If everything is ok we should see this message in the console:
<pre>
2020-06-09 14:21:18.532  INFO 1 --- [           main] c.i.c.l.b.BookinfoRatingsApplication     : Started BookinfoRatingsApplication in 9.326 seconds (JVM running for 10.248)
</pre>

Our application started, but if the pod that contains the postgres container was to fail, we will lose all the data. We need to add some volumes to this container, a special volume called a `PersistentVolumeClaim`. This `claim` made by the pod is a request for storage that Kubernetes will try to fulfill.   
But in order to fulfill this request, we must tell Kubernetes where and how much data it can persist. We must define one or more `PersistentVolume` objects, which will actually store information about the storage, like location(host path or cloud provided storage external to the cluster), access mode(ReadWriteOnce, ReadOnly, etc) and storage class (manual, dynamic, etc). 

When an application makes a `claim`, Kubernetes will look at the `PersistentVolumes`, sees how much space it has left to allocate and if it's the right type of storage class(manual, dynamic, etc) and if any matching storage is found, the `PersistentVolumeClaim` will be bound to the `PersistentVolume`.   

We could also set up dynamically provisioned storage. You can read more about this [here](https://kubernetes.io/docs/concepts/storage/dynamic-provisioning/) . In our example we will store the data on minikube node. 

minikube is configured to persist files stored under the following directories, which are made in the Minikube VM (or on your localhost if running on bare metal). You may lose data from other directories on reboots.

* /data  
* /var/lib/minikube  
* /var/lib/docker  
* /tmp/hostpath_pv  
* /tmp/hostpath-provisioner  

Let's begin by creating a `PersistentVolume` object. `bookinfo-pv.yaml`
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0001
  namespace: bookinfo-ns
spec:
  <b>storageClassName: manual</b>
  accessModes:
    <b>- ReadWriteOnce</b>
  capacity:
    <b>storage: 1Gi</b>
  hostPath:
    <b>path: /data/bookinfo/pv0001/ </b>
</pre>

Access mode can be of the following:  

* ReadWriteOnce – the volume can be mounted as read-write by a single node  
* ReadOnlyMany – the volume can be mounted read-only by many nodes  
* ReadWriteMany – the volume can be mounted as read-write by many nodes  

the `hostPath` is relative to the container/vm that minikube runs in.  
the `storageClassName` indicates the storage class. In this case we manually allocated space on the host. 

Let's create a `PersistentVolumeClaim` now. `bookinfo-ratings-pvc.yaml`
<pre>
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: bookinfo-ratings-pvc
  namespace: bookinfo-ns
spec:
  <b>storageClassName: manual</b>
  accessModes:
    <b>- ReadWriteOnce</b>
  <b>volumeMode: Filesystem</b>
  resources:
    <b>requests:
      storage: 512Mi</b>
</pre>

Now we must define a volume in the database deployment and reference the `PersistentVolumeClaim`. Make the following modifications in `bookinfo-ratings-db.yaml`
<pre class="file" data-target="clipboard">
    spec:
      <b>volumes:
        - name: ratings-storage
          persistentVolumeClaim:
              claimName: bookinfo-ratings-pvc</b>
      containers:
      - name: bookinfo-ratings-db
        image: postgres:13
        env:
        - name: POSTGRES_USER
          value: bnpplearning
        - name: POSTGRES_PASSWORD
          value: whatpassword
        - name: POSTGRES_DB
          value: ratingsdb
        <b>volumeMounts:
          - mountPath: "/var/lib/postgresql/data"
            name: ratings-storage</b>
</pre>

Let's deploy the `PersistentVolume` first to see how it actually works.
<pre>
<b>$ kubectl apply -f bookinfo-kubernetes/bookinfo-pv.yaml</b>
<b>$ kubectl get pv</b>

NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS        CLAIM                STORAGECLASS   REASON   AGE
<b>pv0001                                     1Gi        RWO            Retain           Available                          manual                  3s</b>
</pre>
We see that the persitent volume was created and it's status is `Available`, it was not bound to any `PersistentVolumeClaim` yet.   

Let's create the `PersistenceVolumeClaim` now.
 <pre>
<b>$ kubectl apply -f bookinfo-kubernetes/bookinfo-ratings-pvc.yaml</b>
<b>$ kubectl get pvc
NAME                   STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
bookinfo-ratings-pvc   Bound    pv0001   1Gi        RWO            manual         3s</b>
</pre>

We see that it already was bounded to a volume called pv0001(our PersistentVolume). 
Let's look again at our pv
<pre>
<b>$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS        CLAIM                              STORAGECLASS   REASON   AGE
pv0001                                     1Gi        RWO            Retain           Bound         bookinfo-ns/bookinfo-ratings-pvc   manual                  7m20s</b>
</pre>

The pv status changed to bound. We never specified in the persistent volume claim to which persistent volume to bound to, this way we decoupled the application and the storage which is dependent on the underlying infrastructure(cloud provider, vms, bare metal).

Let's redeploy the database now and test to see if our data is persisted. 
```
$ kubectl apply -f bookinfo-kubernetes
$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
bookinfo-ratings-db-589cfdddcf-rdnbb   1/1     Running   0          27m 
bookinfo-ratings-6129c125125-61a35   1/1     Running   0          27m 
...
$ kubectl port-forward bookinfo-ratings-6129c125125-61a35 8080:8080
```
Now we can make a `POST` request to `http://localhost:8080/ratings-app/ratings` with this body
```
   {
        "bookId": "1",
        "rating": 5,
        "user": "John"
    }
```

You can check if the rating was created with a `GET` request at `http://localhost:8080/ratings-app/ratings/1`

Let's delete the database pod
```
kubectl delete pod bookinfo-ratings-db-589cfdddcf-rdnbb
```

And if we retry the request at `http://localhost:8080/ratings-app/ratings/1` we will see that the data is still there.   

You can read more about storage in Kubernetes [here](https://kubernetes.io/docs/concepts/storage/volumes/)
