1. Get the chart. 
   Clone the `helm-training` branch and go to the bookinfo-practice/bookinfo-ratings directory. You should find there a directory called `bookinfo-ratings-chart`. This is our chart.
<pre class="file" data-target="clipboard">
git clone --branch helm-training https://gitlab.com/cloudacademy1/bookinfo-practice.git
cd bookinfo-practice/bookinfo-ratings
</pre>
2. Explore the chart

    Let's explore the structure of the helm chart and discuss about the different files and directories.  
 
   I. `Chart.yaml` file provides a summary of the chart. It contains information like the chart name, description, version, application version, etc.  
     
   II. In the `values.yaml` file we stored all the default values for the chart's parameters. It's just a simple yaml file, like the one we've created at the previous step in order to pass the values to the chart.
   
   III. `charts` directory. Helm also gives you the option to make chart composition in order to create complex charts. For such a chart we can other put the subcharts in this directory or in a remote location.  We we will have to define in the `Chart.yaml` those dependencies, local or remote. Such a chart is called also an `umbrella chart`. In this course, sub charts are not covered but you can find more info about them [here](https://helm.sh/docs/topics/charts/#chart-dependencies).

   IV. `templates` directory. We will keep in this directory the definition of the Kubernetes objects we need in order to install our applications. They are still yaml files and must respect the yaml indendation rules, but they are actually `Go templates`. Those templates will be processed by Helm when installing the chart, replacing the values with the ones we provide. 
   But with Go templates you can do more than simple text substitution, you can use control structures like `if`, `if...else`, `range`(for..), functions to manipulate values(upperCase, add quotes to value, etc) and you can even have local variables and read files. We will see examples with all of those in the next steps where we will create a chart from scratch. 

   V. `templates\Notes.txt`. the content of this file will be displayed when installing a chart, as you've seen in the previous example with Postgres. This is also a Go template, but Helm will not try to use this as a K8s resource.

This chart already contains all the values that we need to install the `bookinfo-ratings`.  Let's do that now.

<pre class="file" data-target="clipboard">
helm install bookinfo-ratings bookinfo-ratings-chart/
</pre>

Check the application has been deployed correctly

<pre class="file" data-target="clipboard">
kubectl get all
kubectl logs -f bookinfo-ratings-xxxxx
</pre>

Now let's start building a chart from scratch for `bookinfo-reviews`. 

   