IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2085708/raw/master/restore-k8s.sh && chmod +x restore-k8s.sh && source restore-k8s.sh kubernetes-training-step6
</pre>

**1. Namespaces**  
You can split your cluster in multiple, virtual clusters called namespaces. 
This way you can divide your cluster between multiple teams, applications or multiple environments (dev, test, uat, prod). 

All objects in Kubernetes exist in a namespace. 
By default Kubernetes has 3 namespaces: 
  * _default_ -  all Kubernetes requests we make(get, create, apply), by default are executed in this namespace 
  * _kube-system_ - the namespace for objects created by the Kubernetes system
  * _kube-admin_ - as a convention, all objects that are used by the entire cluster reside in this namespace(ex: configurations for the dns resolver)

We can see this in our cluster too:
<pre>
$ kubectl get namespaces
NAME                   STATUS   AGE
<b>default                Active   4d2h</b>
<b>kube-public            Active   4d2h</b>
<b>kube-system            Active   4d2h</b>
kube-node-lease  Active   19h
</pre>

We can create our own namespaces:
Create a `bookinfo-namespace.yaml`:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Namespace
metadata:
  name: bookinfo-ns
</pre>

We just have to specify the namespace name. 

```
$ kubectl apply -f bookinfo-kubernetes/bookinfo-namespace.yaml
$ kubectl get namespaces

NAME                   STATUS   AGE
bookinfo-ns            Active   32s
...
```
We could also use this command without creating a yaml  file:
```
kubectl create namespace bookinfo-ns
```

So far, all of our objects were created in the `default namespace`. Let's move everything in the `bookinfo-ns` namespace. We will begin with `bookinfo-recommendations`. 
<pre>
kubectl apply -f bookinfo-kubernetes/bookinfo-recommendations-deployment.yaml <b>-n bookinfo-ns</b>
</pre>

Notice the `-n bookinfo-ns` at the end. We can specify the target namespace with every request we make(apply, create, delete, get, etc). By default, the target namespace is `default`

Let's see the objects in the `bookinfo-ns`
<pre>
$ kubectl get all -n bookinfo-ns
NAME                                            READY   STATUS    RESTARTS   AGE
pod/bookinfo-recommendations-745c49854c-ffj7r   1/1     Running   0          8s
pod/bookinfo-recommendations-745c49854c-ps52z   1/1     Running   0          8s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bookinfo-recommendations   2/2     2            2           8s

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/bookinfo-recommendations-745c49854c   2         2         2       8s
</pre>

We now have deployed `bookinfo-recommendations` in two separate namespaces.
Let's apply the entire configuration to the new namespace. For this we need to add a `namespace` property to the `metadata` of all our objects
<pre>
metadata:
  ...
  <b>namespace: bookinfo-ns</b>
  ...
</pre>

```
kubectl apply -f bookinfo-kubernetes
# notice we don't specify the namespace here, because we set the namespace in the metadata, Kubernetes will use that as the target namespace 
```

```
$ kubectl get all -n bookinfo-ns
NAME                                            READY   STATUS    RESTARTS   AGE
pod/bookinfo-recommendations-5565f78cbb-nb9pv   1/1     Running   0          12m
pod/bookinfo-recommendations-5565f78cbb-njtxw   1/1     Running   0          12m
pod/bookinfo-ui-594dc5dccd-p4c97                1/1     Running   0          2m6s

NAME                               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
service/bookinfo-recommendations   ClusterIP   10.100.137.163   <none>        3000/TCP       2m6s
service/bookinfo-ui                NodePort    10.108.208.109   <none>        80:31623/TCP   2m6s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bookinfo-recommendations   2/2     2            2           12m
deployment.apps/bookinfo-ui                1/1     1            1           2m6s

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/bookinfo-recommendations-5565f78cbb   2         2         2       12m
replicaset.apps/bookinfo-ui-594dc5dccd                1         1         1       2m6s
```

We have our application deployed in a separate namespace and our services were created here too. So how does Kubernetes knows to resolve the right services IP when we call them by name, because we now have 2 services with the same name, but in different namespaces? 

When  we create a service a new DNS entry is created. This entry is of the form **_service-name_**.**_namespace-name_**.svc.cluster.local . This is the fully qualified domain name(FQDN) but we can use the simple name which is just the **_service-name_**. In this case the name will be resolved in the same namespace. This also means that we can reach services in different namespaces by using the FQDN.

We can also change the default target namespace with this command:
```
kubectl config set-context --current --namespace=bookinfo-ns
```

We can delete now all the objects in the default namespace. When you delete a namespace you delete all the objects in it, but in this case we don't actually want to delete the `default` namespace, just the objects in it. 
```
kubectl delete all --all -n default
```

To delete a namespace you can use
<pre>
kubectl delete namespace <b>namespace-you-want-to-delete</b> 
</pre>

**2. Configure Liveness, Startup and Readiness Probes**  

We saw in chapter 2 that Kubernetes watches our applications, if one of them goes down it will restart it. Kubernetes takes the decision of restarting the application on a liveliness condition, which by default is: `Is the main process in the container still running`? 
This is ok for most cases, but sometimes an application can still be running and not working properly.   

Kubernetes has a way of defining a `liveliness probe` to check the status of each application. 
The liveliness test can be a command to execute in the container, a HTTP request to our pod or we can try to open a new TCP connection with the pod. 

In our `bookinfo-recommendations` we have a endpoint 'recommendations-app/health' which can tell us if the application is still running properly, so we will be using the HTTP liveliness probe. 

We have to modify our `bookinfo-recommendations-deployment.yaml` in order to add the `liveliness probe`. 

<pre class="file" data-target="clipboard">
....
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        imagePullPolicy: Always
        envFrom:
          - secretRef:
                name: librarian-secret
        <b>livenessProbe:
          httpGet:
            path: /recommendations-app/health
            port: 3000
          initialDelaySeconds: 3
          periodSeconds: 3
          failureThreshold: 3</b>
      imagePullSecrets: 
        - name: my-registry-secret
....
</pre>

We add a **`livenessProbe`** object to our container.   
**`httpGet`** - is the type of probe, we use `exec` to just execute  a command in the container and `tcpSocket` for the TCP test.   
**`path`** - the path were the request will be made
**`port`** - the port were the request will be made
**`initialDelaySeconds`** - specifies how many seconds should Kubernetes to wait before running this probe, some applications might start slower
**`periodSeconds`** - how often to test this probe. In this case Kubernetes will test this pod every 3 seconds. 
**`failureThreshold`** - after how many failed tests should Kubernetes restart the container

For the HTTP test, a response code greater or equal to 200 and less than 400 means everything is ok. 

Getting the `initialDelaySeconds` right is pretty hard, especially with older applications that can take some time to start. You might either set a period that is not long enough for the application to start up, ending up with a liveliness probe that is always failing or you could set a period that is too long and the liveliness probe will not start until that period is over. 

What we want is to set a maximum period for our application to start, but if it starts faster than that the liveliness probe should kick in. 


 For this problem the `startup probe` was introduced that does just that. No other probe is executed until this one either exceeds the time limit or gets a OK response. This is way more flexible than just settings a `initialDelaySeconds`
The configuration is similar to the `livelinessProbe` object, the only difference being that there is no `initialDelaySeconds` in neither of the probes now. 
<pre class="file" data-target="clipboard">
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        imagePullPolicy: Always
        envFrom:
          - secretRef:
                name: librarian-secret
        livenessProbe:
          httpGet:
            path: /recommendations-app/health
            port: 3000
          <strike>initialDelaySeconds: 3</strike>
          periodSeconds: 3
          failureThreshold: 3
        <b>startupProbe:
          httpGet:
            path: /recommendations-app/health
            port: 3000
          periodSeconds: 3
          failureThreshold: 30</b>
      imagePullSecrets: 
        - name: my-registry-secret
</pre>

The `initialDelaySeconds` is now actually `failureThreshold` * `periodSeconds`, in this case that means 3 * 30 = 90sec. If our application starts faster than that the liveliness probe kicks in. 

There is another type of probe in Kubernetes, called a `readiness probe`. This probe covers the case when the application started, it can answer to the health requests but is not really ready to receive traffic. Maybe it has to load some configuration first, load a big file into memory or make an external request on startup. 

This is very similar to the other probes and this one too will not start until the `startup probe` is not finished. A pod that fails this probe will not be restarted, but it will no longer receive traffic from a Kubernetes Service(if we have one) and it's status will change to UNREADY. Kubernetes will keep probing the container until is READY again. 

Let's add this probe too. Usually you want a different test than the one for the `liveliness probe` but in this case we will use the same endpoint:
<pre class="file" data-target="clipboard">
...
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        envFrom:
          - secretRef:
                name: librarian-secret
        livenessProbe:
          httpGet:
            path: /recommendations-app/health
            port: 3000
          periodSeconds: 3
          failureThreshold: 3
        startupProbe:
          httpGet:
            path: /recommendations-app/health
            port: 3000
          periodSeconds: 3
          failureThreshold: 3
        <b>readinessProbe:
          httpGet:
            path: /recommendations-app/health
            port: 3000
          periodSeconds: 3
          failureThreshold: 3</b>
....
</pre>

Now we can apply the new configuration:
```
kubectl apply -f bookinfo-kubernetes
```

We can use `describe` on one of the pods to see if the probes were added to our pods. 
<pre>
$ kubectl describe pod bookinfo-recommendations-84fc9b5b6-2cmns
....
Containers:
  bookinfo-recommendations:
    Container ID:   docker://49650f113bebb9688c204a67f0a0f407600e6b7cacaa5e7304ca29bd83457afc
    ...
    State:          Running
      Started:      Mon, 25 May 2020 21:21:51 +0300
    Ready:          True
    Restart Count:  0
    <b>Liveness:       http-get http://:3000/recommendations-app/health delay=0s timeout=1s period=3s #success=1 #failure=3
    Readiness:      http-get http://:3000/recommendations-app/health delay=0s timeout=1s period=3s #success=1 #failure=3
    Startup:        http-get http://:3000/recommendations-app/health delay=0s timeout=1s period=3s #success=1 #failure=3</b>
....
</pre>

You can read more aboute `Probes` [here](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes).  


**3. Quality of Service**

Kubernetes uses QoS classes to make decisions about scheduling and evicting Pods. 

There are 3 classes of QoS: 
  * **`Guaranteed`**
  * **`Burstable`**
  * **`BestEffort`**

  For a pod to have the `Guaranteed` QoS class all the containers in the pod must have the same `request` and `limit` for the cpu and ram.   
  For a pod to have the `Burstable` QoS class at least one container must specify at least `request` or `limit` for cpu or ram.
  A pod with no `request` or `limit` for neither cpu or ram has the  `BestEffort` QoS class.


Let's give our `bookinfo-recommendations` a `Guaranteed`  QoS class.
<pre class="file" data-target="clipboard">
...
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        env:
          - name: LIBRARIAN_NAME
            value: John
        <b>resources:
          requests:
            memory: "400Mi"
            cpu: "300m"
          limits:
            memory: "400Mi"
            cpu: "300m"</b>
...
</pre> 

`requests` values are used at scheduling, while `limits` are used while the container is running

`requests` - is used by Kubernetes to find a suitable node for the pod to live in. There must be a node with this much resources available in order for the pod to be scheduled.   
`limits` - after it was scheduled, if the containers will use more resources than the limit, Kubernetes will kill the container.