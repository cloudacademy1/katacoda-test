The `bookinfo-ratings` is also a `spring-boot` application that connects to a `postgres` database. 

The Dockefile will be very similar to the one we used for `bookinfo-reviews`.You can try this one on your own and then check if you got it right.

```
FROM openjdk:8-jre-alpine
WORKDIR /usr/bin/bookinfo-ratings
COPY target/*.jar /usr/bin/bookinfo-ratings/bookinfo-ratings.jar
ENTRYPOINT ["java", "-jar", "bookinfo-ratings.jar"]
```

Let's build the image. Don't forget that you need to be in the `bookinfo-ratings` directory, where the Dockerfile is. 

Build the Maven project:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    mvn clean package -DskipTests
</span>

Build the docker image:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker build -t bookinfo-ratings:1.0 .
</span>

`bookinfo-ratings` needs a `postgres` instance to store the details about the books. Let's run a `postgres` container attached to the `bookinfo-network`

Create a volume so postgres can persist the data. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker volume create postgres-ratings-volume
</span>

And run the container. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d --name my-postgres-ratings -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=ratingsdb --mount source=postgres-ratings-volume,target=/var/lib/postgresql/data --network bookinfo-network postgres
</span>


And now let's run it. We will attach the container to the network `bookinfo-network`.
We also need to specify the database url. We will use the `postgres` container's name as the hostname for the database url.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -p 3002:8080 --name bookinfo-ratings -e DB_PORT=5432 -e DB_HOSTNAME=my-postgres-ratings -e PORT=8080 --network bookinfo-network  bookinfo-ratings:1.0
</span>

Test the application is running using

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker logs -f bookinfo-ratings
</span>
