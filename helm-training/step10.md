Before proceeding to the exercises, learn about [Variables](https://helm.sh/docs/chart_template_guide/variables/) and [Accessing Files Inside Templates](https://helm.sh/docs/chart_template_guide/accessing_files/) from Helm documentation.

IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2093937/raw/master/restore-helm-step10.sh && chmod +x restore-helm-step10.sh && source restore-helm-step10.sh

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
chmod 700 get_helm.sh && \
./get_helm.sh && \
alias helm=/usr/local/bin/helm
</pre>

Exercise 1: <b>Variables</b>

Let's take again the deployment we used in previous exercise. We see that <b>.Values.podLabels.service</b> is used twice, in selectors and metadata section. Instead of duplicating the reference, let's keep its value in a variable.
<pre class="file" data-target="clipboard">
<b>{{- $serviceLabel := .Values.podLabels.service }}</b>
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app }}
    tier: {{ .Values.labels.tier }}
    version: {{ .Release.Name }}-{{ .Release.Revision }}
spec:
  replicas: {{ .Values.replicasCount }}
  selector:
    matchLabels:
      <b>service: {{ $serviceLabel }}</b>
  template:
    metadata:
      labels:
        <b>service: {{ $serviceLabel }}</b>
    spec:
      containers:
        - name: {{ .Values.containerName }}
          image: {{ .Values.imageName }}
          env:
            - name: DB_PORT
              value: "5432"
            - name: DB_HOSTNAME
              value: bookinfo-reviews-db
            - name: PORT
              value: "8080"
          volumeMounts:
          {{- range $vm := .Values.volumes }}
            - mountPath: {{ $vm.mountPath }}
              name: {{ $vm.name }}
              {{- if hasKey $vm "subPath" }}
              subPath: {{ $vm.subPath }}
              {{- end }}
          {{- end }}

      volumes:
        {{- range $vol := .Values.volumes }}
        - name: {{ $vol.name }}
          {{- if hasKey $vol "configMap" }}
          configMap:
            name: {{ $vol.configMap }}
          {{- end }}
          {{- if and $vol.isEmptyDir }}
          emptyDir: {}
          {{- end }}
        {{- end }}
</pre>


Exercise 2: <b>Accessing Files Inside Templates</b>

Let's take our configMap for this exercise. We see that we have a list of properties in the data section, the same properties that we previously had in our application.properties file. So could we use these properties from the old application.properties file in our configMap, instead of copying them directly in the template? The answer is yes, but we have to make some modification in our configMap definition.

First of all, let's add the application.properties file inside the <b>bookinfo-reviews-chart</b> folder. The content of the file should be:

<pre class="file" data-target="clipboard">
spring.application.name=BookInfo-Reviews
server.port=${PORT:8080}
server.servlet.context-path=/reviews-app
spring.jpa.hibernate.ddl-auto=none
spring.flyway.locations=classpath:db/migration
spring.devtools.remote.secret=superSecret
</pre>


Next, let's modify our configMap template to read the properties from this file:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: ConfigMap
metadata:
    name: "configmap-{{ .Values.appName }}"
    namespace: {{ .Values.namespace }}
data:
<b>  {{- $files := .Files }}
  {{- range tuple "application.properties"}}
  {{ . }}: |-
{{ $files.Get . | indent 6}}
  {{- end }}</b>
</pre>

If you run this template, the result should be the same as before, with the only difference that the data is read from the application.properties file.