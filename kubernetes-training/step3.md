IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2085708/raw/master/restore-k8s.sh && chmod +x restore-k8s.sh && source restore-k8s.sh kubernetes-training-step3
</pre>

**1. Create a deployment yaml**  

<i>Note:</i> We will be using `bookinfo-practice` folder as the root folder for all the commands in this exercise, so please navigate to this directory.

A `Deployment` is a Kubernetes object that manages multiple versions or revisions. 
A deployment is the idea of associating a new revision to a new replicaset and manage all the replicasets created this way through a single object. This way you can control which replicaset(revision) is active and how the transition from one replicaset to another is made.

Let's create a new yaml file 'bookinfo-recommendations-deployment.yaml' in the `bookinfo-practice` folder. The yaml files for our replicaset and our deployment are very similar
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  name: bookinfo-recommendations
  labels:
    app: bookinfo
    tier: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      service: recommendations
  template:
    metadata:
      labels:
        service: recommendations
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:1.0
        env:
          - name: LIBRARIAN_NAME
            value: John
</pre>

The only difference here is the **`kind`** attribute which is in this case `Deployment`. But let's apply this to the cluster to see what will actually create.  
  
**2. Creating the deployment**  
```
kubectl apply -f bookinfo-recommendations-deployment.yaml
```
To see all the objects we can do 
```
$ kubectl get all
NAME                                            READY   STATUS    RESTARTS   AGE
pod/bookinfo-recommendations-7d9d76bb8c-nj9q8   1/1     Running   0          18s
pod/bookinfo-recommendations-7d9d76bb8c-q8sqb   1/1     Running   0          18s

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/bookinfo-recommendations-7d9d76bb8c   2         2         2       18s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bookinfo-recommendations   2/2     2            2           18s
```

We see that we created a deployment, a replicaset and 2 pods. Let's take a closer look at the replicaset that was created.   
  
**3. Taking a closer look to the deployment** 

```
kubectl describe rs bookinfo-recommendations-7d9d76bb8c
```

<pre>
Name:           bookinfo-recommendations-7d9d76bb8c
Namespace:      default
Selector:       pod-template-hash=7d9d76bb8c,service=recommendations ➊
Labels:         pod-template-hash=7d9d76bb8c ➋
                service=recommendations
Annotations:    deployment.kubernetes.io/desired-replicas: 2 ➌
                deployment.kubernetes.io/max-replicas: 3 ➍
                deployment.kubernetes.io/revision: 1 ➎
Controlled By:  Deployment/bookinfo-recommendations  ➏
Replicas:       2 current / 2 desired
.......
</pre>

 ➊ and ➋ - Kubernetes added a label to our replicaset called pod-template-hash which is actually the hash of the template object in our deployment. Any modification in the template object will trigger a new revision so a new replica will be created too. This label is also used as a selector. Kubernetes will associate each revision with a template hash and this way it identifies which replicaset is the right one for each revision 

➌ and ➍ and ➎ - more information for Kubernetes to check on the replicaset if it behaves correctly 

➏ - just like we've seen with pods controlled by a replicaset, this replicaset is controlled by a deployment  

Now let's take a look at our deployment 
```
kubectl describe deployment.apps/bookinfo-recommendations
```
```
Name:                   bookinfo-recommendations
Namespace:              default
CreationTimestamp:      Fri, 22 May 2020 18:40:15 +0300
Labels:                 app=bookinfo
                        tier=backend
Annotations:            deployment.kubernetes.io/revision: 1 ➊
                        kubectl.kubernetes.io/last-applied-configuration:
                          {"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"labels"...
Selector:               service=recommendations
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate ➋ 
MinReadySeconds:        0 ➌ 
RollingUpdateStrategy:  25% max unavailable, 25% max surge ➍
Pod Template:
  Labels:  service=recommendations
  Containers:
   bookinfo-recommendations:
    Image:      cloudacademyibmro/bookinfo-recommendations:1.0
    Port:       <none>
    Host Port:  <none>
    Environment:
      LIBRARIAN_NAME:  John
    Mounts:            <none>
  Volumes:             <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none> ➎
NewReplicaSet:   bookinfo-recommendations-7d9d76bb8c (2/2 replicas created) ➏
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  31m   deployment-controller  Scaled up replica set bookinfo-recommendations-7d9d76bb8c to 2
```

➊ - it indicates that this deployment is currently at revision (version) 1.

➋ - switching from one revision to another requires a strategy for replacing the old pods, controlled by the old replicaset with the new pods, controlled by the new replicaset. There are multiple strategies you can use, but a Deployment only supports 2 types of its own, without other Kubernetes objects:   
  * **Recreate** - is the simplest strategy. First, all the old pods will be destroyed, when all of them terminated, the new ones are started. This will cause some downtime and is not recommended
  * **RollingUpdate** - is the default strategy. One of the new pods is started, when it is ready, one of the old ones is destroyed and the process repeats until all the pods are replaced.   
    
For more information about other strategies for updates check this [article](https://dzone.com/articles/kubernetes-deployment-strategies) (ignore the Weaveworks Flagger part)   
  
➌ - during a rolling update, if a new pod is ready for 'MinReadySeconds' specified than it will be available to receive traffic.  

➍ With this 2 properties you can set the RollingUpdate strategy speed
  * max unavailable - The maximum number of pods that can be unavailable during the update
  * max surge - The maximum number of pods that can be scheduled above the desired number of pods    
  
➎ the replicaset that is being replaced, this property will have a value only when an update is in progress 

➏ the current replicaset active  

 
  

**4. Rolling a new version of the app**  
Let's try to update our application by adding a new book to our recommendations.   
Modify the `index.js` in the bookinfo-recommendations and add "Kubernetes in Action" to the `programmingBooks` array (line 30);
<pre>
const programmingBooks = ["The Pragmatic Programmer", "Clean Code", "Introduction to Algorithms", <b>"Kubernetes in Action"</b>];
</pre>
<i>Hint: use the IDE tab to open and edit index.js file from the project you cloned in step 1 - the path should be root/bookinfo-practice/bookinfo-recommendations.</i>
</br>
*If you are on Katacoda, skip the image build and use the images already built from cloudacademyibmro repository.   
Let's now rebuild the image. Please notice the new tag:
<pre>
docker build -t bookinfo-recommendations:<b>2.0</b> bookinfo-recommendations
</pre>

Now we must modify our deployment to update the version of the image we are using
<pre>
spec:
  containers:
    - name: bookinfo-recommendations
      <b>image: cloudacademyibmro/bookinfo-recommendations:2.0</b>
</pre>

Let's deploy this new configuration to Kubernetes
```
kubectl apply -f bookinfo-recommendations-deployment.yaml
```

Let's see what objects we have now
```
$ kubectl get all
NAME                                           READY   STATUS    RESTARTS   AGE
pod/bookinfo-recommendations-99789f596-866s9   1/1     Running   0          26s
pod/bookinfo-recommendations-99789f596-9ssgb   1/1     Running   0          21s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   27h

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bookinfo-recommendations   2/2     2            2           88m

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/bookinfo-recommendations-7d9d76bb8c   0         0         0       88m
replicaset.apps/bookinfo-recommendations-99789f596    2         2         2       26s
```

We see that we now have 2 replicasets, but one of the has the desired number of pods set to 0. That is the replicaset that corresponds to our old deployment. 

Let's look at the new replicaset
<pre>
...
Labels:         <b>pod-template-hash=99789f596</b>
                service=recommendations
Annotations:    deployment.kubernetes.io/desired-replicas: 2
                deployment.kubernetes.io/max-replicas: 3
                <b>deployment.kubernetes.io/revision: 2</b>
....
  Containers:
   bookinfo-recommendations:
    <b>Image:      cloudacademyibmro/bookinfo-recommendations:2.0</b>
</pre>

We see that this replicaset has a different template hash, the annotation now says revision 2 and we also see the image tag was updated. 

You can now do a port-forwarding to one of the pods and check if the new book shows up.   

**5. Revision Management**  
We can see the history of our revisions
```
$ kubectl rollout history deployment bookinfo-recommendations

deployment.apps/bookinfo-recommendations 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
```

We can rollback to the previous revisions
```
kubectl rollout undo deployment bookinfo-recommendations
```

Or we can rollout a specific revision, let's say we want to change back to revision 2
```
kubectl rollout undo deployment bookinfo-recommendations --to-revision=2
```
  
**5. Scaling a deployment**
    
A deployment can be scaled the same way as a replicaset, the scaling will be applied to the currently active replicaset. 

```
kubectl scale --replicas=4 deployment bookinfo-recommendations
```

```
kubectl get pods
```



You can read more about the `Deployment` object [here](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)  
For the next exercise you must clean the cluster again
```
kubectl delete deployment bookinfo-recommendations
```

