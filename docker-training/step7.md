Until now, we ran our bookinfo-reviews application with h2.
We can also run this application with postgres.

In this exercise we will run 2 containers:
1. bookinfo-reviews
2. postgres database

This step requires step 6 to be completed. 

In the previous steps you learned that docker containers run in different networks
than the ones from the local machine. This is why we use port-forwarding to access
the applications inside containers.

How can we connect 2 containers if they have isolated networks? We can create
a shared network!. Docker offers this functionality. 

Let's create our shared network:
<pre class="file" data-target="clipboard">
docker network create bookinfo-network
</pre>

You can check that the network was created using:

`docker network ls`

Now that we have a common network we have to start the containers with this network configured.

Let's first start the postgres container:

<pre class="file" data-target="clipboard">
docker run -d --network bookinfo-network -e POSTGRES_PASSWORD=password --name my-postgres-reviews postgres
</pre>

Now if we look in bookinfo-reviews `bookinfo-practice/bookinfo-reviews/src/main/resources/application-postgres.properties`
we can observe a few things:

We can configure a postgres connection by defining the following environment variables:
1. DB_HOSTNAME
2. DB_PORT
3. DB_USER
4. DB_PASS

We need to somehow tell docker to define these variables when starting the bookinfo-reviews container.
Fortunately `docker run` offers the `-e` to define env variables inside the running container.
We also need to start spring boot with `postgres` profile.

We can now start our application container with a postgres connection:

<pre class="file" data-target="clipboard">
docker run -it -d -p 8080:8080 -e DB_HOSTNAME=my-postgres-reviews -e DB_PORT=5432 -e DB_USER=postgres -e DB_PASS=password \
--network bookinfo-network --name bookinfo-reviews bookinfo-reviews:1.0 --spring.profiles.active=postgres
</pre>

Observe that `DB_HOSTNAME` uses the name of the postgres container.
`--spring.profiles.active=postgres` acts as an extra parameter to the Dockerfile entrypoint:
`ENTRYPOINT ["java", "-jar", "bookinfo-reviews.jar"]`. This way we can specify the spring `postgres` profile.

Go ahead and create a review using the shell script:
`bookinfo-practice/bookinfo-reviews/src/main/resources/create-sample-review.sh`

If everything is ok, now we could open a terminal inside the database container and try to connect to the database.
<pre class="file" data-target="clipboard">
    docker exec -it my-postgres-reviews /bin/bash
</pre>

To connect to the database execute(-h host, -p port, -U username):
<pre class="file" data-target="clipboard">
    psql -h localhost -p 5432 -U postgres
</pre>

Enter the password of the database.

List the databases:
<pre class="file" data-target="clipboard">
\l
</pre>

Connect to `postgres` database:
<pre class="file" data-target="clipboard">
\c postgres
</pre>

List available tables:
<pre class="file" data-target="clipboard">
\dt
</pre>

You should have an output like this:
```
Schema |         Name          | Type  |  Owner   
--------+-----------------------+-------+----------
 public | flyway_schema_history | table | postgres
 public | reviews               | table | postgres
 ```

You can execute a select to check that your review was indeed saved to this database:

<pre class="file" data-target="clipboard">
select * from reviews;
</pre>

If you executed the script:
`bookinfo-practice/bookinfo-reviews/src/main/resources/create-sample-review.sh` you should have an output like this:

```
postgres=# select * from reviews;
              review_id               | book_id |  review_content  | reviewer |       review_date       
--------------------------------------+---------+------------------+----------+-------------------------
 0b2dd1d6-bc76-43a8-8814-0a01431dc3ec | 1       | Such a cool book | John Doe | 2021-02-22 12:06:15.642
(1 row)
```

Remove all containers:
<pre class="file" data-target="clipboard">
docker rm -f $(docker ps -a -q)
</pre>