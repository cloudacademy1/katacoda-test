Sometimes we might misconfigure helm charts or have typos.

How should we check that the values are replaced correctly?


If you want to check how k8s resources will look after parsing the templates you can use(**should be in bookinfo-reviews chart folder**):
<pre class="file" data-target="clipboard">
helm install --dry-run --debug bookinfo-reviews ./ 
</pre>

This command will print all the resources with their values replaced.
This command will not install the resources but will still use k8s api to validate the 
resources.

If you just need to check the templates without a k8s cluster available you can use:
<pre class="file" data-target="clipboard">
helm template --debug bookinfo-reviews ./
</pre>

This command just renders the templates without validating the resources configuration with
a k8s cluster.

To better understand the difference stop the minikube cluster and try to execute the previous
2 commands. What happens?

Start again minikube cluster.

Another way to analyse helm chart is to use `helm lint <chart_path>`. Helm lint is like sonar for helm charts.
It will reveal possible issues with the chart. You don't need an available kubernetes cluster for
this command.

Execute the command:
<pre class="file" data-target="clipboard">
helm lint ./
</pre>