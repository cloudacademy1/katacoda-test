Let's containerize the `bookinfo-recommendations` application. This is a simple server application written in `node` and no database.

We will start from a node image:

`FROM node:14-slim`

Let's set a working directory:

`WORKDIR '/usr/src/bookinfo-recommendations`

Let's first copy just the `package.json` and `package-lock.json`. We will explain in a second why we do this.

`COPY package*.json ./`

Now let's install the dependencies:

`RUN npm install`

And copy the rest of the source code:

COPY . .

We did this steps in this order because we want Docker to cache the npm_modules dependencies of the application. Each COPY, ADD or RUN command in the Dockerfile acts as a layer and it can reuse this layers. So how does it work: 

 * we copy the package.json -> Docker creates this layer and adds it to the image
 * we install the dependencies -> Docker creates another layer and adds it to the image
 * we copy the source code -> Docker creates a third layer and adds it to the image too
  
 If we change something in the source code and want to rebuild the image the following will happen:
 * we copy the package.json -> Docker see that is the same file as before, unchanged, so it uses the layer from the cache
 * we install the dependencies -> because the cache didn't got a miss yet, it copies the layer from cache, without actually running the command again
 * we copy the source code -> now it sees that there are some modifications, so we have a cache miss, every layer after this one will be recreated

If we copied the source code before installing the dependecies, every modification to the source code would have made Docker to invalidate the cache and rebuild the layer where we install the dependencies. 

With the last command in the Dockerfile we tell Docker how to run the container:

`CMD ["npm", "run", "start"]`


This is what the file should look like in the end:

<pre>
FROM node:14-slim
WORKDIR '/usr/src/bookinfo-recommendations'
COPY package*.json ./
RUN npm install
COPY . .
CMD ["npm", "run", "start"]
</pre>

Let's build the image. Don't forget that you need to be in the `bookinfo-recommendations` directory, where the Dockerfile is. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker build -t bookinfo-recommendations:1.0 .
</span>

And now let's run it. We will attach the container to the network you've created earlier `bookinfo-network`.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -p 3001:3000 --name bookinfo-recommendations --network bookinfo-network bookinfo-recommendations:1.0
</span>

Test the application by running

`curl http://localhost:3001/recommendations-app/programming`{{execute}}

or 

`curl http://localhost:3001/recommendations-app/cooking`{{execute}}
