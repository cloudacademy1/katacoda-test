Before proceeding to the exercises, learn about [Named Templates](https://helm.sh/docs/chart_template_guide/named_templates/) from Helm documentation.

IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2094005/raw/master/restore-helm-step11.sh && chmod +x restore-helm-step11.sh && source restore-helm-step11.sh

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
chmod 700 get_helm.sh && \
./get_helm.sh && \
alias helm=/usr/local/bin/helm
</pre>

Exercise 1: <b>Declaring and using templates with define and template</b>

We already have the file called _helpers.tpl, created together with the chart. That file is the default location for template partials. Let's take again the deployment we used in previous exercises and create a partial for it, storing the environments part.

First, let's add the partial in _helpers.tpl:

<pre class="file" data-target="clipboard">
{{/*
Generate app env properties 
*/}}
{{- define "bookinfo-reviews.env" }}
          env:
            - name: DB_PORT
              value: "5432"
            - name: DB_HOSTNAME
              value: bookinfo-reviews-db
            - name: PORT
              value: "8080"
{{- end }}
</pre>

Now we can embed this template inside of our existing Deployment, and then include it with the template action:
 
<pre class="file" data-target="clipboard">
{{- $serviceLabel := .Values.podLabels.service }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app }}
    tier: {{ .Values.labels.tier }}
    version: {{ .Release.Name }}-{{ .Release.Revision }}
spec:
  replicas: {{ .Values.replicasCount }}
  selector:
    matchLabels:
      <b>service: {{ $serviceLabel }}</b>
  template:
    metadata:
      labels:
        <b>service: {{ $serviceLabel }}</b>
    spec:
      containers:
        - name: {{ .Values.containerName }}
          image: {{ .Values.imageName }}
          <b>{{- template "bookinfo-reviews.env" }}</b>
          volumeMounts:
          {{- range $vm := .Values.volumes }}
            - mountPath: {{ $vm.mountPath }}
              name: {{ $vm.name }}
              {{- if hasKey $vm "subPath" }}
              subPath: {{ $vm.subPath }}
              {{- end }}
          {{- end }}

      volumes:
        {{- range $vol := .Values.volumes }}
        - name: {{ $vol.name }}
          {{- if hasKey $vol "configMap" }}
          configMap:
            name: {{ $vol.configMap }}
          {{- end }}
          {{- if and $vol.isEmptyDir }}
          emptyDir: {}
          {{- end }}
        {{- end }}
</pre>


Exercise 2: <b>The include function</b>

Take a look again at the template partial that we created before: you can see that there is lot of indentation before the "env" key:
<pre class="file" data-target="clipboard">
{{/*
Generate app env properties 
*/}}
{{- define "bookinfo-reviews.env" }}
          env:
            - name: DB_PORT
              value: "5432"
            - name: DB_HOSTNAME
              value: bookinfo-reviews-db
            - name: PORT
              value: "8080"
{{- end }}
</pre>

If you change this indentation, the template will not work. But keeping it in the _helpers.tpl file may produce errors in case someone changes the values and messes with the indentation. In order to be more maintainable, we should take care of the indentation right in the place where we need the template to be replaced, like we did before in our exercises. But for this, we would need to call the <b>indent</b> function. Because template is an action, and not a function, there is no way to pass the output of a template call to other functions; using template, the data is simply inserted inline.

To work around this case, Helm provides an alternative to template that will import the contents of a template into the present pipeline where it can be passed along to other functions in the pipeline.

Let's modify the deployment as follows:
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app }}
    tier: {{ .Values.labels.tier }}
    version: {{ .Release.Name }}-{{ .Release.Revision }}
spec:
  replicas: {{ .Values.replicasCount }}
  selector:
    matchLabels:
      service: {{ .Values.podLabels.service }}
  template:
    metadata:
      labels:
        service: {{ .Values.podLabels.service }}
    spec:
      containers:
        - name: {{ .Values.containerName }}
          image: {{ .Values.imageName }}
          <b>{{- include "bookinfo-reviews.env" . | indent 10 }}</b>
          volumeMounts:
          {{- range $vm := .Values.volumes }}
            - mountPath: {{ $vm.mountPath }}
              name: {{ $vm.name }}
              {{- if hasKey $vm "subPath" }}
              subPath: {{ $vm.subPath }}
              {{- end }}
          {{- end }}

      volumes:
        {{- range $vol := .Values.volumes }}
        - name: {{ $vol.name }}
          {{- if hasKey $vol "configMap" }}
          configMap:
            name: {{ $vol.configMap }}
          {{- end }}
          {{- if and $vol.isEmptyDir }}
          emptyDir: {}
          {{- end }}
        {{- end }}
</pre>

Now, you can remove the extra indentation from the template partial:
<pre class="file" data-target="clipboard">
{{/*
Generate app env properties 
*/}}
{{- define "bookinfo-reviews.env" }}
env:
  - name: DB_PORT
    value: "5432"
  - name: DB_HOSTNAME
    value: bookinfo-reviews-db
  - name: PORT
    value: "8080"
{{- end }}
</pre>

Run again and the result should be the same as before.