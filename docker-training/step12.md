We will containerize our last application `bookinfo-ui`. 

We will use  something called `multi-stage builds` in order to do have a final smaller image.

In a `multi-stage Dockerfile` we can define more then one image that needs to be build, but only the final one will be our real image. We do this so we can build intermmediate images in which we can do the build and keep the final image smaller by just copying in the final image the result of the build. 

Let's start our first image that will be our intermediate image.

`FROM node:14-slim as builder` 

Notice that we gave a name to this intermmediate image. We called it `builder` but it can be anything we want.

We would use the same trick as we did for the other applications in order for Docker to cache some layers.

```
WORKDIR '/usr/src/bookinfo'
COPY package*.json ./
RUN npm install
COPY . .
```

And now let's run the build of the Angular application.
`RUN npm run build`

Now let's create our final image. We will be starting from the `nginx` image this time since we don't need node to run the application, we only need it for the build.

`FROM nginx:1.17.10-alpine as prod`

Notice that we also gave a name to this image, `prod`, but this is not required.
Now let's copy our build files from the `builder` image.

`COPY --from=builder /usr/src/bookinfo/dist/bookinfo-ui /usr/share/nginx/html`

And now let's copy our nginx config file.

`COPY nginx.conf /etc/nginx/conf.d/default.conf`

We don't need to specify the `CMD` here since it can use the command that is in the `nginx` image. It just need to start the http server.

Our final Dockerfile should look like this:

```
FROM node:14-slim as builder 
WORKDIR '/usr/src/bookinfo'
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.17.10-alpine as prod
COPY --from=builder /usr/src/bookinfo/dist/bookinfo-ui /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
```

If we build this Dockerfile only the last image in the Dockerfile will get built, the rest will be discarded by Docker.

Let's build the image. Don't forget that you need to be in the `bookinfo-ui` directory, where the Dockerfile is. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker build -t bookinfo-ui:1.0 .
</span>

And now let's run it. We will attach the container to the network `bookinfo-network`.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -p 4200:80 --name bookinfo-ui --network bookinfo-network  bookinfo-ui:1.0
</span>

You can access `http://localhost:4200` in your browser or if you are on the Katacoda environment you can test it here https://[[HOST_SUBDOMAIN]]-4200-[[KATACODA_HOST]].environments.katacoda.com/

You should see a button called `init database`. Press it in order for the application to insert some dummy data.