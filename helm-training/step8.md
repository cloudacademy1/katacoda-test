Before proceeding to the exercises, learn about [Template Functions and Pipelines](https://helm.sh/docs/chart_template_guide/functions_and_pipelines/) from Helm documentation.


IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2093659/raw/master/restore-helm-step8.sh && chmod +x restore-helm-step8.sh && source restore-helm-step8.sh

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
chmod 700 get_helm.sh && \
./get_helm.sh && \
alias helm=/usr/local/bin/helm
</pre>

Exercise 1: <b>quote</b> function

As a first exercise, we can start with a best practice: When injecting strings from the .Values object into the template, we ought to quote these strings. We can do that by calling the quote function in the template directive. Let's use this function in the service template we previously created:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ <b>quote</b> .Values.labels.app }}
    tier: {{ <b>quote</b> .Values.labels.tier }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.service.targetPort }}
      protocol: TCP
      name: http
  selector:
      service: {{ <b>quote</b> .Values.podLabels.service }}
</pre>

Exercise 2: <b>Pipelines</b>

Let's rewrite the service template using a pipeline:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app <b>| quote</b> }}
    tier: {{ .Values.labels.tier <b>| quote</b> }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.service.targetPort }}
      protocol: TCP
      name: http
  selector:
      service: {{ .Values.podLabels.service <b>| quote</b> }}
</pre>

Exercise 3: <b>default</b> function

Let's modify the service by replacing the hardcoded value for a port's protocol with a parametrized value, but in case this parameter is not defined in .Values file, keep the default "TCP" value. 
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app | quote }}
    tier: {{ .Values.labels.tier | quote }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.service.targetPort }}
      protocol:  {{ .Values.service.protocol | default "TCP" | quote }}
      name: http
  selector:
      service: {{ .Values.podLabels.service | quote }}
</pre>

Run this without adding <i>service.protocol</i> key in .Values file - the result is the same as before, dafault "TCP" value is used. 

Now, add <i>service.protocol</i> key in .Values file and set its value to "UDP" (another supported protocol in Kubernetes). If you run now, the value "UPD" is filled for service port protocol.

You can remove the <i>service.protocol</i> key in .Values file, because it was only used for exemplification, the correct value for our template is "TCP", so we can keep the default one.

Exercise 4: <b>toYaml</b> function

Let's add some new labels for our service in metadata section: appCode and environment. The simplest way to do this is to just add them directly in the service template file, like this:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app | quote }}
    tier: {{ .Values.labels.tier | quote }}
    <b>appCode: {{ .Values.labels.appCode | quote }}
    environment: {{ .Values.labels.environment | quote }}</b>
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.service.targetPort }}
      protocol:  {{ .Values.service.protocol | default "TCP" | quote }}
      name: http
  selector:
      service: {{ .Values.podLabels.service | quote }}
</pre>

Add <i>labels.appCode</i> and <i>labels.environment</i> keys in .Values file and run it - you'll have the expected result. But in order to achieve it, you had to add the new labels in two files: in the template and in .Values file. And if you want to add the new labels for the other templates (deploymnet and configmap), you have to manually add them in these files too.

Instead of explicitly declaring the labels in the template, we can use <b>toYaml</b> function to add all the keys and values under a specific key in .Values file:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
<b>{{ toYaml .Values.labels | indent 4 }}</b>
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.service.targetPort }}
      protocol:  {{ .Values.service.protocol | default "TCP" | quote }}
      name: http
  selector:
      service: {{ .Values.podLabels.service | quote }}
</pre>

We have also used here the <b>indent</b> function to indent the labels with 4 spaces.

Run it now and you'll have all the labels, just like before.

Let's now add a new label: version. The only thing that you need to do now is to add the <i>labels.version</i> in .Values file and run again - the new label will be present in the result.

You can apply these exercises on the other templates too - deployment and configmap.