Open `values.yaml`.

Let's start with some global values that can be used to identify
the k8s resources:

<pre class="file" data-target="clipboard">
#-----------Global values---------#
appName: bookinfo-reviews
namespace: default
labels:
  app: bookinfo
  tier: backend
</pre>

The names of these variables are related to k8s's configuration
resources properties. E.G. `labels` are k8s labels used to identify
the application.
You can name the variables however you want but recommended practice is
to name them after k8s properties.

Let's specify image to be deployed:

<pre class="file" data-target="clipboard">
#-----------Deployment values---------#
replicasCount: 1
podLabels:
  service: reviews

containerName: bookinfo-reviews
imageName: cloudacademyibmro/bookinfo-reviews:1.0
</pre>

Let's configure the k8s service:
<pre class="file" data-target="clipboard">
#----------Service values---------#
service:
  type: NodePort
  port: 8080
  targetPort: 8080
</pre>

Configure the external properties that will be used by the application:

<pre class="file" data-target="clipboard">
#----------ConfigMap values---------#
configmap:
  spring:
    application:
      name: BookInfo-Reviews
    jpa:
      hibernate:
        ddl_auto: none
    flyway:
      locations: classpath:db/migration
    devtools:
      remote:
        secret: superSecret
  server:
    port: ${PORT:8080}
    servlet:
      context_path: /reviews-app
</pre>

Configure the volume for config map properties file:
<pre class="file" data-target="clipboard">
#---------Volumes values---------#
volume:
  name: application-external-config
  mountPath: /usr/bin/bookinfo-reviews/config/
  configMap: configmap-bookinfo-reviews
</pre>

Now that we defined all the possible configuration that our application
can have we can move onto the k8s resources in the `templates` folder.

Let's start with `templates/deployment.yaml`.
Delete the previous content of this file as we will use a simpler
config.

### Deployment k8s resource configuration

1. Start with the metadata:

<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app }}
    tier: {{ .Values.labels.tier }}
    version: {{ .Release.Name }}-{{ .Release.Revision }}
</pre>

---
**SIDE NOTE**

Observe the usage of Release object. Even though, we don't have a
file `Release.yaml` helm still injects this objects in our template
to be used for retrieving release information as release name that is
passed as an argument when installing a helm chart.

---

We can observe a weird syntax: `{{ .Something }}`
This is a syntax from the Golang language, more specifically 
_**Go Templates**_. Basically this is how we tell helm to replace
variables with values from _**values.yaml**_. To better understand
think about how bash variable substitution works: "${VARIABLE}". Or
how angular variable substitution works: 
```html
<div [hidden]="!itemForm.form.valid">
  <p>{{ submitMessage }}</p>
</div>
```

This is the same concept, but it has a wide variety of operations
you can perform, not only variable substitution. We will see
in the next exercises how to exploit the capabilities of Go Templates.

For now here is a cheatsheet with the most used operations:
[Go Templates cheatsheet](https://lzone.de/cheat-sheet/Helm%20Templates)

Back to our `deployment.yaml`...

2. Configure the spec:
<pre class="file" data-target="clipboard">
spec:
  replicas: {{ .Values.replicasCount }}
  selector:
    matchLabels:
      service: {{ .Values.podLabels.service }}
  template:
    metadata:
      labels:
        service: {{ .Values.podLabels.service }}
    spec:
      containers:
        - name: {{ .Values.containerName }}
          image: {{ .Values.imageName }}
          volumeMounts:
            - mountPath: {{ .Values.volume.mountPath }}
              name: {{ .Values.volume.name }}

      volumes:
        - name: {{ .Values.volume.name }}
          configMap:
            name: {{ .Values.volume.configMap }}
</pre>

All the placeholders are from `values.yaml`.

We finished the deployment resource.
Let's continue with the service resource.

Clear the content of `service.yaml`

### Service k8s resource configuration

1.Configure the metadata:

<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app }}
    tier: {{ .Values.labels.tier }}
</pre>

2.Configure the spec:

<pre class="file" data-target="clipboard">
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: {{ .Values.service.targetPort }}
      protocol: TCP
      name: http
  selector:
      service: "{{ .Values.podLabels.service }}"
</pre>

Now we also need to create a configmap resource for our externalized
spring boot configuration file. Helm does not create a skeleton for
k8s config map resource, so we have to create the file ourselves.
Create in the `templates` folder a file called `configmap.yaml`.

### Configmap k8s resource configuration

1.Configure the metadata:

<pre class="file" data-target="clipboard">
apiVersion: v1
kind: ConfigMap
metadata:
    name: "configmap-{{ .Values.appName }}"
    namespace: {{ .Values.namespace }}
</pre>

2.Configure the data:

<pre class="file" data-target="clipboard">
data:
  application.properties: |
    spring.application.name: {{ .Values.configmap.spring.application.name }}
    server.port: {{ .Values.configmap.server.port }}
    server.servlet.context-path: {{ .Values.configmap.server.servlet.context_path }}
    spring.jpa.hibernate.ddl-auto: {{ .Values.configmap.spring.jpa.hibernate.ddl_auto }}
    spring.flyway.locations: {{ .Values.configmap.spring.flyway.locations }}
    spring.devtools.remote.secret: {{ .Values.configmap.spring.devtools.remote.secret }}
</pre>

Finally, we finished our helm chart configuration. For simplicity,
bookinfo-reviews app uses an in memory database.

Let's install it! Navigate to bookinfo-reviews base folder and
execute the command:
<pre class="file" data-target="clipboard">
helm install bookinfo-reviews bookinfo-reviews-chart
</pre>

An output similar to this should appear:
```shell
NAME: bookinfo-reviews
LAST DEPLOYED: Sun Mar 21 16:57:05 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
Thank you for installing bookinfo-reviews-chart.

Your release is named bookinfo-reviews.

To learn more about the release, try:

  $ helm status bookinfo-reviews
  $ helm get all bookinfo-reviews
```

We can verify that our pod is running by executing:
<pre class="file" data-target="clipboard">
kubectl get pods
</pre>

Identify the pod name and do a port forwarding to test the application with
swagger:

<pre class="file" data-target="clipboard">
kubectl port-forward bookinfo-reviews-5df78d4b87-lh7qm  8080:8080
</pre>

Access swagger to test the application:

[Swagger](https://[[HOST_SUBDOMAIN]]-8080-[[KATACODA_HOST]].environments.katacoda.com/reviews-app/swagger-ui.html)

Uninstall the chart for now:
<pre class="file" data-target="clipboard">
helm uninstall bookinfo-reviews
</pre>

Verify that no pods are running.

Helm can also help with multi environments deployments as stated in the previous
steps.

`values.yaml` contain default values for our deployment but what
if we want some values to be different for another environment?

As we saw in the previous exercises we can specify configuration values
or files when installing helm charts. 
Let's change the context root for our application.

Create a file in the helm chart folder called `values-dev.yaml` in the same folder where `values.yaml` is located.
Fill in the following configuration:

<pre class="file" data-target="clipboard">
#----------ConfigMap values---------#
configmap:
  server:
    port: ${PORT:8080}
    servlet:
      context_path: /cool-reviews
</pre>

Now install the chart with the following command(navigate to `bookinfo-reviews-chart` folder):
<pre class="file" data-target="clipboard">
helm install -f values-dev.yaml bookinfo-reviews ./
</pre>

Helm will override the default properties from `values.yaml`
with the ones from `values-dev.yaml`, in our case this is only
the servlet context.

Do a kubectl port forward.
Execute a curl to test the new context-root:
<pre class="file" data-target="clipboard">
curl http://localhost:8080/cool-reviews/swagger-ui.html
</pre>

Uninstall the chart.





