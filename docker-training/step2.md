<h2>Differences between a container and an image</h2>

**An image** is a filesystem bundle, **a set of files** organized in the same way that Linux organizes it's files.

Those files are our application files, the application dependencies, kernel modules and everything that the operating system needs to run our application process. 

**A container** is just another **process** from the operating system perspective. 

But this process is treated differently than a normal process.  
  * It can't see the other processes that are running on the host operating system
  * Can't see and access files other than the ones included in the image from which was started 
  * Has it's own virtual network and communicates with the host operating system network as it would with a external network.
  * Has access only to a part of the system resources

A container process it's completely isolated from the other processes and has very strict access to the other resources. 

So a container behaves in the same way as a virtual machine, but without all the overhead of a virtual machine. Is quick to create and start.  

For a in-depth explanation you can check this [presentation](https://www.youtube.com/watch?v=wyqoi52k5jM)


**You can think of a image as a .exe file and of a container as the process that the operating system starts once you execute the .exe file.**

<hr>
For our second container let's try to run an custom image, created for this exercise and already pushed to a public registry, that prints a hello message every second.

You can run this container using:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker run cloudacademyibmro/message-app-demo:1.0
</span>

Compared to the first container which just printed the message and exited, this one runs in an infinite loop and now our terminal is blocked. This is not very helpful, what we want is to run this container as a background process, or daemon.

To do this we can add `-d`(detached) flag after the run command (you can stop the already running container using CTRL + C).

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker run <b>-d</b> cloudacademyibmro/message-app-demo:1.0
</span>

When we run this container as detached the docker client only gets back from the docker daemon the id that was assigned to the container and not the output stream. 

<hr>
In order to see all the containers that are running we can use:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker ps
</span>

You should see an output similar to this:
```
CONTAINER ID  IMAGE     COMMAND    CREATED  STATUS  PORTS  NAMES
a9108b11b30c  cloud…   "./mess…"   16 sec…  Up 16…         kind_leakey
```

* CONTAINER ID -  **id** that the docker daemon assign to the container
* IMAGE - the **image** from which the container was created
* COMMAND - the **command used to start the main process** inside the container
* NAMES - random and unique name that the docker daemon assigned to the container, can be also used to identify the container in a human friendly manner

We can specify the name of the container if we want, instead of letting the daemon random generate it.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker run -d <b>--name my-message-app-demo</b> cloudacademyibmro/message-app-demo:1.0
</span>

If you run `docker ps` again you will see that now you have 2 containers.
<hr>
Now let's try to remove the first one. To do this you have to either specify the id or the name of the container.
In my case, I will use the id, for you is probably a different id. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker stop a9108b11b30c
</span>

You probably observed that we said `stop` and not `remove`. And Docker actually didn't remove the container, like you would send a virtual machine into hibernate mode. What it actually  did was to stop the process, but left the container if you want to restart it.

To see all the containers, running or not, we could add `-a` flag to the `ps` command.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker ps -a
</span>

You should now see 4 containers, 1 that are running and 3 containers that exited(hello-world container, the first message-app-demo that we stopped with ctrl+C and the one we stopped using `docker stop`).

If you want to remove permanently the container you can use `remove` on a stopped container, but not a running container, you have to stop it first.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker rm a9108b11b30c
</span>


<hr>
The script that is executed inside the container is:
<pre class="file" data-target="clipboard">
#!/bin/sh

while true
do
    echo "Hello $NAME"
    sleep 1
done
</pre>

You see that script should output "Hello" followed by the value of the environment variable "NAME". But this variable was not defined when we created the container. We can pass environment variables to the container when we start it using the `-e` flag

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker run -d -e NAME=John cloudacademyibmro/message-app-demo:1.0
</span>

Because we used also the `-d` flag we cannot see the messages from the output stream of the container. 
But Docker has a command for this problem too. We can reattach to the outputstream of the container temporarily. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker logs -f <i>id_or_name_of_the_container</i>
</span>

You should see now the message "Hello John" in the console. The `-f` flag means to follow the logs. 

<hr>
We said that a container is similar to a virtual machine. We could connect to the container like we would to a virtual machine using ssh. To see this better you can execute `pwd` or `ps` before executing the next docker command. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
  docker exec -it <i>id_or_name_of_the_container /bin/sh</i>
</span>

You should see that the prompt in the terminal changed and all the commands we would execute from now on will be only affecting the files and processes inside the container. if you run again `pwd` or `ps` you would see that you are in a different directory and you have a different list of processes. You can also try `ls` to see the structure of the files inside the container, they are arranged to look like the Linux filesystem. 

To return to your terminal execute `exit`.

You are not actually connecting like you would do with ssh. What the docker does is to attach a child process, the sh terminal in this case, to the container process. You can start like as many process you want in the container. 


[Here](https://dockerlabs.collabnix.com/docker/cheatsheet/) is a cheatsheet for the most usual commands in docker.

In the next step we will run a Postgres container.