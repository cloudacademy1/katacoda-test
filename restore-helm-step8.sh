#!/bin/bash
echo "Waiting to initialize with previous step progress..."
#restoring repo state
git clone --branch helm-training-step8 https://gitlab.com/cloudacademy1/bookinfo-practice.git

#restoring working directory
cd bookinfo-practice/

#restore minikube
minikube status | grep "host: Running\nkubelet: Running\napiserver: Running\nkubeconfig: Configured" &> /dev/null
if [[ $? != 0 ]]; then
   minikube start
fi

echo "Done"
