IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2085708/raw/master/restore-k8s.sh && chmod +x restore-k8s.sh && source restore-k8s.sh kubernetes-training-step4
</pre>

**1. Deploying the second component of our app**  

<i>Note:</i> We will be using `bookinfo-practice` folder as the root folder for all the commands in this exercise, so please navigate to this directory.

Before we create a Kubernetes Service let's try to understand why we need it in the first place.  

Let's create a new folder called `bookinfo-kubernetes` in the `bookinfo-practice` folder and copy here only the deployment that we created earlier 'bookinfo-recommendations-deployment.yaml'.
This will be the folder where we keep our yaml files from now on. 

*If you are on Katacoda, skip the image build and use the images already built from cloudacademyibmro repository.

Let's create an image for another component of our app `bookinfo-ui`. 
```
docker build -t bookinfo-ui:1.0 bookinfo-ui
```
Let's build a new Deployment for it `bookinfo-ui-deployment.yaml` in the `bookinfo-kubernetes` folder (don't forget to change the image name with your username)
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  name: bookinfo-ui
  labels:
    app: bookinfo
    tier: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      service: angular-ui
  template:
    metadata:
      labels:
        service: angular-ui
    spec:
      containers:
      - name: bookinfo-ui
        image: cloudacademyibmro/bookinfo-ui:1.0
</pre>

Now we can execute `kubectl apply` for the folder `bookinfo-kubernetes`
```
kubectl apply -f bookinfo-kubernetes
```

And let's see what objects were created
```
$ kubectl get all
NAME                                           READY   STATUS    RESTARTS   AGE
pod/bookinfo-recommendations-99789f596-kvkrc   1/1     Running   0          71s
pod/bookinfo-recommendations-99789f596-m84js   1/1     Running   0          71s
pod/bookinfo-ui-5766dc5785-z56pn               1/1     Running   0          71s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bookinfo-recommendations   2/2     2            2           71s
deployment.apps/bookinfo-ui                1/1     1            1           71s

NAME                                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/bookinfo-recommendations-99789f596   2         2         2       71s
replicaset.apps/bookinfo-ui-5766dc5785               1         1         1       71s
```

We have 2 deployments, 2 replica sets and 3 pods (2 bookinfo-recommendations(1 if you scale down earlier on Katacoda) and 1 bookinfo-ui)   
The `bookinfo-ui` pod does have an IP address, but is not accessible from outside the cluster so we have to use port-forward. 
```
kubectl port-forward pod/bookinfo-ui-5766dc5785-z56pn 4200:80
```
If we access the application from the browser `localhost:4200` we see that it does not work, no books are displayed.   

If you are running on Katacoda, as explained in step1, you have two options for the request: 
1. open a new terminal and use curl to see the result
2. you can change the default binding to `0.0.0.0`.  The command is: `kubectl port-forward bookinfo-ui 4200:80 --address 0.0.0.0` . Then you can access from the browser at: https://[[HOST_SUBDOMAIN]]-4200-[[KATACODA_HOST]].environments.katacoda.com

The application that is running in the `bookinfo-ui` pod is supposed to call the `bookinfo-recommendations` app to get the list of recommended books but it does not know how to reach it, what's his IP address. We can get the `bookinfo-recommendations` IP and just use that from the `bookinfo-ui` but what happens if `bookinfo-recommendations` goes down and comes back up but with a different IP? Also which one of the `bookinfo-recommendations` instances should we use? How do we balance the traffic between them?   

**2.Use cases for Kubernetes Services**  
This are 3 of the problems that a `Service` wants to resolve.   
How do applications find each other inside the cluster?  
How do we load balanced the traffic between the pods of the same deployment?  
How do we expose our application outside of the cluster, to the users?   

A service is an object that stays in front of a group of pods and all traffic to the pods go through the service that also does load balancing between the pods.  
The service itself has an IP address that can be used to call the applications behind it. We can install inside the cluster a DNS server to resolve the service name to the service IP address so we can just call it by name. Minikube, KinD and most of the cloud providers come with a DNS server by default so we don't have to do anything to set that up. 
A service can be one of four types:  
  * ClusterIP - accesible only inside the cluster
  * NodePort - exposes an IP address outside the cluster
  * LoadBalancer - creates a service that uses an external load balancer, this is usually provided by the cloud vendor
  * ExternalName - this type of service stays in front of an external service that a pod inside the cluster might call. Maybe the database is not running inside the Kubernetes cluster or you have to call a 3rd party API, you can create a service in front of that and call the service instead from  your pod. 


**3. Creating a ClusterIP Service**
Let's start with a ClusterIP, we want such a service for our `bookinfo-recommendations` which should be accessible only inside the cluster, to be consumed by `bookinfo-ui`. We will start by creating a file 'bookinfo-recommendations-service.yaml'
```
apiVersion: v1
kind: Service
metadata:
  name: bookinfo-recommendations
spec:
  selector:
    service: recommendations
  ports:
    - port: 3000
```

* **`metadata`** we specify the service name which will also be used as a domain name, so instead of using the service IP we can call by this name
* **`selector`** we select the pods with the label 'service: recommendations'
* **`ports`** the port that our service will listen on, by default it will proxy traffic to the pods on the same port. If we want to expose a port through a service but the pods are actually listening on another port we have to specify a property called `targetPort` so that traffic arrives on the right port in the pods.   

If we do not specify a type for our service, by default, it will be a ClusterIP Service. 

Let's update our cluster 
```
kubectl apply -f bookinfo-kubernetes
```
<pre>
$ kubectl get services
NAME                       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
<b>bookinfo-recommendations   ClusterIP   10.104.175.66   <none>        3000/TCP   88m</b>
kubernetes                 ClusterIP   10.96.0.1       <none>        443/TCP    30h
</pre>

Our service is now created. We see it's type, ClusterIP, it's IP address and the port that is listening on.    
Ignore the 'kubernetes' service, is created by minikube/KinD by default.

Let's taste again the application by using port-forwarding
```
kubectl port-forward bookinfo-ui-9b85d4cf-9mvvh 4200:80
``` 

If you open the browser on localhost:4200 the 'Get recommendations' feature should work now.   

**3. Creating a NodePort Service**
Let's create a service that will expose our app externally.
Create `bookinfo-ui-service.yaml`
```
apiVersion: v1
kind: Service
metadata:
  name: bookinfo-ui
spec:
  selector:
    service: angular-ui
  ports:
    - port: 80
  type: NodePort
```

It's very similar to the ClusterIP service, we just specified the type `NodePort`. `bookinfo-ui` pod has the label 'service: angular-ui' and listens on port 80 so we had to change that too. 

```
kubectl apply -f bookinfo-kubernetes
```
<pre>
$ kubectl get services
NAME                       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
bookinfo-recommendations   ClusterIP   10.104.175.66   <none>        3000/TCP       97m
<b>bookinfo-ui                NodePort    10.111.253.94   <none>        80:32226/TCP   4m57s</b>
kubernetes                 ClusterIP   10.96.0.1       <none>        443/TCP        31h
</pre>

To get the address of our app we can run `minikube service bookinfo-ui --url`. 
This will actually give us the minikube node IP and the nodePort: 'http://minikube_node_ip:nodePort'

For Katacoda you won't be able to access the application using the url from minikube. You have to take the port and replace it in this url: https://[[HOST_SUBDOMAIN]]-PORT_HERE-[[KATACODA_HOST]].environments.katacoda.com/

For KinD the IP can be retrieved with `kubectl get node kind-control-plane -o jsonpath="{.status.addresses[0].address}{'\n'}"`   and the port can be retrieved with `kubectl get service bookinfo-ui -o jsonpath="{.spec.ports[0].nodePort}{'\n'}"`
We will see next why we can't use the service IP address.

**5. A closer look at Services**
  
Let's take a closer look at our services. Let's try with the ClusterIP.
<pre>
$ kubectl describe service bookinfo-recommendations

Name:              bookinfo-recommendations
Namespace:         default
Labels:            <none>
Annotations:       kubectl.kubernetes.io/last-applied-configuration:
                     {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"name":"bookinfo-recommendations","namespace":"default"},"spec":{"ports":...
Selector:          service=recommendations
Type:              ClusterIP  
IP:                10.104.175.66  ➊
Port:              <unset>  3000/TCP
TargetPort:        3000/TCP  ➋  
Endpoints:         172.18.0.5:3000,172.18.0.6:3000  ➌
Session Affinity:  None ➍
Events:            <none>
</pre>

➊ the IP address that was allocated to our Service 

➋ the service will forward the requests to this port on the backend pods   

➌ this is the list of pods IPs that are going to be load balanced by this service 

➍ you can set this property to ClientIP if you want to create a sticky session(once a client request is sent to a pod, all subsequent requests from that client will be sent to the same pod)  

Now let's look at the NodePort Service
```
$ kubectl describe service bookinfo-ui

...
Type:                     NodePort
IP:                       10.111.253.94
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  32226/TCP ➊
...
```

We see that it look almost the same as the ClusterIP, it receives traffic on port 80 and sends it forward to the pods on the target port, 80. So traffic coming from inside the cluster can still be received on port 80. 
However, it does have another port called nodePort(➊), that will be accesible from outside the cluster. You can specify this port, but Kubernetes can manage this for us so we don't have to deal with eventual port conflicts. 

A NodePort is actually just a ClusterIP and can be used as one but we still can't use the service IP to access it from outside the cluster. We have to use one of the nodes IPs. It doesn't matter which node, because the kube-proxy component will take care of routing the request to the correct location. 

How it actually works?  
Let's say we have a cluster with 3 nodes(machines), each one with an IP accessible from the internet. Also, each one of those nodes have a process running called a kube-proxy. Each kube-proxy knows about all the services in the whole cluster.
By creating a NodePort we are doing 2 things: 
1. Create a ClusterIP 
2. Tell all the kube-proxies to listen on the nodePort specified. 

When a request is made, the kube-proxy looks for the ClusterIP service that is associated with the nodePort and sends the traffic there.


You can read more about services [here](https://kubernetes.io/docs/concepts/services-networking/service/)