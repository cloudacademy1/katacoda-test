Now that we have a running container and we can connect to it from the host machine let's add some data in there.


This small script will create a schema, a table in that schema and it will insert two rows. You can execute it directly from the host terminal using psql. 

`psql -h localhost -p 5432 -U postgres`{{execute}}

`
create schema test;
create table test.user(id int, name varchar(20));
insert into test.user(id, name) values (1, 'John');
insert into test.user(id, name) values (2, 'Adam'); 
select * from test.user;
\q
`{{execute}}

Let's see what happens when we delete the container and restart it. 

<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    1. docker stop my-database && docker rm my-database
</div>
<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    2. docker run -d -e POSTGRES_PASSWORD=password --name my-database <b>-p 5432:5432</b> postgres
</div>

Let's try to do a `SELECT` again. 

`
psql -h localhost -p 5432 -U postgres -c "SELECT * FROM TEST.USER;"
`{{execute}}

Our schema, table and data is gone. Once we remove a container, all the files associated with it are gone. Containers are supposed to be disposable so that if it encounters an error it can't recover from(e.g., deadlock) we can quickly recreate a new one. 

So in order to use containers in applications that are supposed to be stateful, like a database, we can make use of **`bind mounts`** or **`volumes`**. 

Let's start with a **`bind mount`**. When running a container we can specify to Docker that when data is being read or written to a specific location on the container, that it should  actually read or write that data from/to another specific location, but on the host machine, outside of the container. 

**`Volumes`** are very similar to the `bind mounts`, the difference being that you don't choose the path of the host machine that we want to use. That location will be chosen and managed by Docker(usually is /var/lib/docker/volumes/). Docker does this in order to avoid arbitrarily choosing a location that might be shared with other processes. **Using volumes is the preffered way**. 

Let's see an example with both of the approaches.
A volume can be created using the command:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker volume create my-volume
</span>

In order to see all the existing volumes or more information about a specific volume you can use: 

<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    1. docker volume ls
</div>
<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    2. docker volume inspect my-volume
</div>

<pre>
    [
        {
            <b>"Driver": "local",</b>
            "Labels" {},
            <b>"Mountpoint": "/var/lib/docker/volumes/my-volume/_data",</b>
            "Options": {},
            "Scope": "local"
        }
    ]
</pre>

You can see that in the **"Mountpoint"** is the path where the volume will be mapped. This means that all the data that is being read or written by a container will be from this location on the host machine. 

Let's check what we have now in this location. It should be empty.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    ls /var/lib/docker/volumes/my-volume/_data
</span>


Let's mount this volume to our postgres container. Postgres writes the data in `/var/lib/postgresql/data` so that's where we should mount the volume we've created. It could have been any location on the container. Only the read/write operations in the specified container's location will be persisted in the volume.

<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    1. docker stop my-database && docker rm my-database
</div>
<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    2. docker run -d -e POSTGRES_PASSWORD=password --name my-database -p 5432:5432 <b>--mount source=my-volume,target=/var/lib/postgresql/data</b>  postgres
</div>

Let's execute the create and insert script again and then restart the container.

`psql -h localhost -p 5432 -U postgres`{{execute}}

`
create schema test;
create table test.user(id int, name varchar(20));
insert into test.user(id, name) values (1, 'John');
insert into test.user(id, name) values (2, 'Adam'); 
select * from test.user;
\q
`{{execute}}

<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    1. docker stop my-database && docker rm my-database
</div>
<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    2. docker run -d -e POSTGRES_PASSWORD=password --name my-database -p 5432:5432 <b>--mount source=my-volume,target=/var/lib/postgresql/data</b>  postgres
</div>

Let's try to do a `SELECT` again. 

`
psql -h localhost -p 5432 -U postgres -c "SELECT * FROM TEST.USER;"
`{{execute}}

Now our data was saved. Postgres still writes the data in `/var/lib/postgresql/data` but this time it will be persisted on the host machine too, in `/var/lib/docker/volumes/my-vol/_data`. 

Only the read/write operations in the specified container's location, in this case `/var/lib/postgresql/data`,  will be persisted in the volume.

Binding mounts are used in a similar way. The difference is that instead of using a volume as a target, we will specify directly the path on the host machine where we want to persist the data. 


<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -e POSTGRES_PASSWORD=password --name my-database -p 5432:5432 <b>--mount source=/some/path/on/the/host/machine,target=/var/lib/postgresql/data</b>  postgres
</div>

Let's check again the path where Docker created the volume.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    ls /var/lib/docker/volumes/my-volume/_data
</span>

Now we see all the files that Postgres created here when it created the database and we inserted the data.

<hr>
You should remember that application data can be written outside the container. Either using volumes, in a specific area of the host's filesystem or bind mounts, anywhere on the host's filesystem.
![Manage application data](https://docs.docker.com/storage/images/types-of-mounts.png)

Source: [Docker](https://docs.docker.com/storage/)

<hr>
You can also persist data from a container to another external machine or even the cloud. When we inspected the volume we saw a property called `Driver`, that in our case was local. But we can create volumes with different drivers that knows how to store data anywhere. We will talk more about this feature in the Kubernetes course where we will want to store data in a cloud file storage system. 


If you want to dig deeper into volumes and how to persist data from containers you can check [the Docker documentation](https://docs.docker.com/storage/).







