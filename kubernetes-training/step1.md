For a brief introduction of kubernetes access this link: [Kubernetes intro](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)

For these exercises we are going to use the same **bookinfo** application with this architecture:

![bookinfo](https://i.ibb.co/0M6JR1X/full-example.png)

We already have all the Dockerfiles created from the previous exercise.

Clone the application (we will be using bookinfo-practice folder as the root folder for all the commands):
<pre class="file" data-target="clipboard">
git clone --branch kubernetes-training https://gitlab.com/cloudacademy1/bookinfo-practice.git
cd bookinfo-practice
</pre>

### I. Deploy the first component to Kubernetes
We will start with `bookinfo-recommendations`, since is the simplest and has no dependecies.   
We need to prepare the image before deploying it to Kubernetes.
  
  
*If you are on Katacoda, skip the image build and use the images already built from cloudacademyibmro repository. (Go to `3. Create a Kubernetes cluster`)

**1. Build the image**
```
docker build -t bookinfo-recommendations:1.0 bookinfo-recommendations
```

**2. Push the image to the `DockerHub`**  
First, you need to create a DockerHub account, then you can execute 
```
docker login
```
It will ask you for the username and the password. After this step is complete you should see the message `Login Succeeded`  
Next we need to tag the image with our repository and push it. 
<pre class="file" data-target="clipboard">
docker tag bookinfo-recommendations:1.0 <b>username</b>/bookinfo-recommendations:1.0
docker push <b>username</b>/bookinfo-recommendations
</pre>

Where **username** is your DockerHub username, for example: 
```
docker tag bookinfo-recommendations:1.0 cloudacademyibmro/bookinfo-recommendations:1.0
docker push cloudacademyibmro/bookinfo-recommendations
```
**3. Create a Kubernetes cluster**

We can choose either Minikube or KinD. Minikube is more stable.

If you are doing this exercise on Katacoda you already have installed `minikube`


**With Minikube**  
minikube recommends using a hypervisor or Docker to run the cluster.  This example is using Docker to run the cluster, but you can specify other hypervisors.   
For Linux: `KVM`, `VirtualBox`  
For Windows: `Hyper-V`, `VirtualBox`  
For Mac: `HyperKit`, `VirtualBox`  
[more info](https://kubernetes.io/docs/setup/learning-environment/minikube/#specifying-the-vm-driver) 
```
$ minikube start #for Katacoda
$ minikube start --driver=docker #for Linux

😄  minikube v1.10.1 on Redhat 7.7
✨  Using the docker driver based on user configuration
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.18.2 preload ...
    > preloaded-images-k8s-v3-v1.18.2-docker-overlay2-amd64.tar.lz4: 525.43 MiB
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.18.2 on Docker 19.03.2 ...
    ▪ kubeadm.pod-network-cidr=10.244.0.0/16
🔎  Verifying Kubernetes components...
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"
```

**With KinD**  
`KinD` stands for `Kubernetes in Docker` and it will start a Kubernetes cluster in a docker container. [more info](https://kind.sigs.k8s.io/)  
To create a cluster we execute:
<pre class="file" data-target="clipboard">
$ <b>kind create cluster</b>  
 ✓ Ensuring node image (kindest/node:v1.18.2) 🖼 
 ✓ Preparing nodes 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️ 
 ✓ Installing CNI 🔌 
 ✓ Installing StorageClass 💾 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Thanks for using kind! 😊
</pre>


We can check the `kubectl` is connected to the right cluster
<pre class="file" data-target="clipboard">
<b>For Minikube</b>
$ kubectl cluster-info
Kubernetes master is running at https://172.17.0.3:8443
KubeDNS is running at https://172.17.0.3:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

<b>For KinD</b>
$ kubectl cluster-info
Kubernetes master is running at https://127.0.0.1:28150

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
</pre>
We just created a cluster with a single node which is also the master node.
<pre class="file" data-target="clipboard">
<b>For Minikube</b>
$ kubectl get nodes
NAME       STATUS   ROLES    AGE     VERSION
minikube   Ready    master   9m47s   v1.18.2

<b>For KinD</b>
$ kubectl get nodes
NAME                 STATUS   ROLES    AGE    VERSION
kind-control-plane   Ready    master   6m2s   v1.18.2
</pre>

**4. Create a pod**   
We start by creating a file  called `bookinfo-recommendations-pod.yaml`. We will start with the simplest configuration possible. If you execute the commands on the local environment and want to use your own image, don't forget to replace the image name with the one from your repository. If you are running on Katacoda, there should be a second tab on the right side where you have an IDE. Create this file in the root of the project. 
<pre class="file" data-target="clipboard">
apiVersion: v1 
kind: Pod 
metadata: 
  name: bookinfo-recommendations  
  labels:  
    app: bookinfo
    tier: backend
    service: recommendations
spec: 
  containers:
  - name: bookinfo-recommendations
    image: cloudacademyibmro/bookinfo-recommendations:1.0
</pre>

**`apiVersion`** specifies which version of the Kubernetes API you’re using to create this object. We want to create a Pod, one of the core Kubernetes objects that was released with v1(the first stable release). The most common Kubernetes objects were  introduced in either `v1` or `apps/v1`. [Here](https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-apiversion-definition-guide.html) is a list with the most commons objects and the apiVersion for each one.
  
**`kind`** specifies the type of object you want to create, in our case a `Pod`   
   
**`metadata`** represents data that helps uniquely identify the object. A `name` or `UID` is required. We can also add extra information like `labels`, which is a key-value list of arbitrary data  

**`spec`** is the description of the object we want to create. The structure of the **spec** is different from object to object, in this case, for a `Pod` we specified the:  
   *  **`containers`** - a list of the containers that we want run in this pod. For each container we specify the name for the container and the image, which will be the image that we push earlier to DockerHub

To deploy this to our cluster we execute:
```
kubectl create -f bookinfo-recommendations-pod.yaml
```

We have check if our application was deployed. To see all the pods in our cluster we execute:
```
$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
bookinfo-recommendations   1/1     Running   0          46m
```

We can see we have a pod named bookinfo-recommendations, 1/1 under the `READY` columns means that the pod has one container and that container is running. For example, If we have 2 containers, and only one started, we would see in 1/2 in the `READY` column.  
We will see in the chapter about Kubernetes probes how we can pass Kubernetes our own definition of `READY` for our app. 
  
**5. Port forwarding into the pod**  
Next we want to check if our application is actually running correctly.The application is running on port 3000. For now we will use port-forwarding to access the application. We can forward the traffic from a port of the host machine to the Kubernetes pod with this command:  
```
kubectl port-forward bookinfo-recommendations 3001:3000
```
In this case we are forwarding traffic from localhost:3001 to the bookinfo-recommendations pod on port 3000.
Now we can use curl or the browser to make a request to `http://localhost:3001/recommendations-app/all` 

If you are running on Katacoda, the environment has some limitations. By default, the port-forward command binds to 127.0.0.1, but you can't access that address from the browser if you are on Katacoda. 

You have two options: 
1. open a new terminal and use curl to see the result
2. you can change the default binding to `0.0.0.0`.  The command is: `kubectl port-forward bookinfo-recommendations 3001:3000 --address 0.0.0.0` . Then you can access from the browser at: https://[[HOST_SUBDOMAIN]]-3001-[[KATACODA_HOST]].environments.katacoda.com/recommendations-app/all

**6. Check the logs for a pod**  
We can check the logs with:
```
$ kubectl logs bookinfo-recommendations

> bookinfo-recommendations@1.0.0 start /usr/src/bookinfo-recommendations
> node index.js

Hello! My name is undefined
Recommending a book for localhost:3001
```
**7. Settings environment variables**  
You can see that there is a bug. The message in the logs says 'My name is undefined', that's because the application needs  a environment variable called **LIBRARIAN_NAME**. Let's change the yaml file to pass this value to the container. 

<pre class="file" data-target="clipboard">
...
containers:
  - name: bookinfo-recommendations
    image: cloudacademyibmro/bookinfo-recommendations:1.0
    <b>env:
      - name: LIBRARIAN_NAME
        value: John</b>
</pre>

We need to delete the pod and recreate it. 
```
kubectl delete pod bookinfo-recommendations
kubectl create -f bookinfo-recommendations-pod.yaml
kubectl get pods
```
If you check the logs again you can see that our problem is fixed.
<pre class="file" data-target="clipboard">
$ kubectl logs pod/bookinfo-recommendations 

> bookinfo-recommendations@1.0.0 start /usr/src/bookinfo-recommendations
> node index.js

<b>Hello! My name is John</b>
</pre>
  
**8. More information about the pod**  
To get more information about our pod we can run `kubectl describe pod bookinfo-recommendations`  

This command works for every Kubernetes object:

<pre class="file" data-target="clipboard">kubectl describe <b>type</b> <b>name</b></pre>
   or 
<pre class="file" data-target="clipboard">kubectl describe <b>type</b>/<b>name</b></pre>
 It will give you more info about that object.


The result is pretty verbose but it will give us a more in depth look at our pod.
<pre class="file" data-target="clipboard">
Name:         bookinfo-recommendations ➊ <i>the name of the pod</i>
Namespace:    default ➋ <i>We can group Kubernetes objects into namespaces, by default all objects that we create go into the default namespace, but we can create and specify other namespaces.</i>
Priority:     0
Node:         minikube/172.17.0.3 ➌ <i>this is the node that our pod was scheduled on to run</i>
Start Time:   Thu, 21 May 2020 17:52:52 +0300 
Labels:       app=bookinfo ➍ <i>we can see the labels we set</i>
              tier=backend
Annotations:  <none>
Status:       Running
IP:           172.18.0.4 ➎ <i>our pod gets automatically an IP adress that is exposed to all the other Kubernetes objects in the same namespace, but you can't reach it from outside the cluster</i>
IPs:
  IP:  172.18.0.4
Containers: ➏ <i>more information about the containers that run inside the pod</i>
  bookinfo-recommendations:
    Container ID:   docker://08aec2840b63ec96dcbb2ac038b254cddbb075ce126a3ae87b603933d87689e7
    Image:          cloudacademyibmro/bookinfo-recommendations:1.0
    Image ID:       docker-pullable://cloudacademyibmro/bookinfo-recommendations@sha256:9506c9068a7ed424cb67892b59d82fa0667a239fdc2cf0290c4514624edb40da
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Thu, 21 May 2020 17:52:53 +0300
    Ready:          True
    Restart Count:  0
    Environment:
      LIBRARIAN_NAME:  John
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-ndqv5 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-ndqv5:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-ndqv5
    Optional:    false
QoS Class:       BestEffort ➐ <i>Quality of Service. We can change this by specifying cpu and memory limits to our pod. This way will be easier for Kubernetes to balance the workload on our cluster. We will see how we can improve this in another chapter</i>
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events: ➑ <i>Here are the events related to our Pod, this section is a very good starting point for debugging a deploy since we can see any error events related to the object</i>
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  10m   default-scheduler  Successfully assigned default/bookinfo-recommendations to minikube
  Normal  Pulled     10m   kubelet, minikube  Container image "cloudacademyibmro/bookinfo-recommendations:1.0" already present on machine
  Normal  Created    10m   kubelet, minikube  Created container bookinfo-recommendations
  Normal  Started    10m   kubelet, minikube  Started container bookinfo-recommendations
</pre>

**9. Run a shell inside a container**  
Next, let's try to run a shell inside the container of our pod.
```
kubectl exec -it bookinfo-recommendations -- bash
``` 
If you have multiple containers in a pod you must also specify the container name. In this example we have only one container in the pod so we don't have to do that. 
Now we can run `ls -latr` and see our application inside the container.

We deployed the app but until now we haven't really let Kubernetes manage our pods. We created and deleted them manually. If we want to start more than one, we can't. Also we don't take advantage of Kubernetes self healing abilities, this means that if our application was to go down, we would have to manually restart the pod. To fix this problem we will use a `ReplicaSet` object. 

Before doing that we must clean up the cluster:
```
kubectl delete pod bookinfo-recommendations
```
