Before proceeding to the exercises, learn about [Flow Control](https://helm.sh/docs/chart_template_guide/control_structures/) from Helm documentation.

IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2093924/raw/master/restore-helm-step9.sh && chmod +x restore-helm-step9.sh && source restore-helm-step9.sh

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
chmod 700 get_helm.sh && \
./get_helm.sh && \
alias helm=/usr/local/bin/helm
</pre>

Exercise 1: <b>If/Else</b>

Let's modify our service and decide the value of the service port based on a condition named <i>service.sslEnabled</i> : if this is false, we'll use for the port the <i>service.targetPort</i> value, as before, but if it is true, we'll use a new value named <i>service.httpsTargetPort</i>. We can also do the same for service name: if sslEnabled is true, the name will be "https", else we keep the name "http".

Let's start by adding the two new values <i>service.sslEnabled</i> that we'll set to true for the sake of the example and <i>service.httpsTargetPort</i> that we'll set to 443. 

The snippet from .Vslues file related to service values should look like this: 
<pre>
........

#----------Service values---------#
service:
  type: NodePort
  port: 8080
  targetPort: 8080
  protocol: TCP
  sslEnabled: true
  httpsTargetPort: 443

.........
</pre>
Now, let's modify the service template:
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Service
metadata:
  name: service-{{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
<b>{{ toYaml .Values.labels | indent 4 }}</b>
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: <b>{{ if and .Values.service.sslEnabled }} {{ .Values.service.httpsTargetPort }} {{ else }} {{ .Values.service.targetPort }} {{ end }}</b>
      protocol:  {{ .Values.service.protocol | default "TCP" | quote }}
      name: <b>{{ if and .Values.service.sslEnabled }} https {{ else }} http {{ end }}</b>
  selector:
      service: {{ .Values.podLabels.service | quote }}
</pre>

You can now set sslEnabled to false or remove it from .Values file, as for our chart we need the http values.

Exercise 2: <b>Looping with the range action</b>

Our deployment has for the moment only one volume, which is enough for our application, because it is a simple one, but in real life there will be several volumes needed for an application. So let's simulate this real-life scenario and add a new volume for an emptyDir. 

The simplest way to do this, will be to add the volume in the deployment template file and the corresponding keys in the values file. But, same discussion as with adding a new label in previous exercise: if a new volume should be added again, we still have to modify again both files. Let's instead addapt the template to iterate over the volumes defined in .Values file:
<pre class="file" data-target="clipboard">
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.labels.app }}
    tier: {{ .Values.labels.tier }}
    version: {{ .Release.Name }}-{{ .Release.Revision }}
spec:
  replicas: {{ .Values.replicasCount }}
  selector:
    matchLabels:
      service: {{ .Values.podLabels.service }}
  template:
    metadata:
      labels:
        service: {{ .Values.podLabels.service }}
    spec:
      containers:
        - name: {{ .Values.containerName }}
          image: {{ .Values.imageName }}
          env:
            - name: DB_PORT
              value: "5432"
            - name: DB_HOSTNAME
              value: bookinfo-reviews-db
            - name: PORT
              value: "8080"
          volumeMounts:
          <b>{{- range $vm := .Values.volumes }}</b>
            - mountPath: {{ $vm.mountPath }}
              name: {{ $vm.name }}
              {{- if hasKey $vm "subPath" }}
              subPath: {{ $vm.subPath }}
              {{- end }}
          <b>{{- end }}</b>

      volumes:
        <b>{{- range $vol := .Values.volumes }}</b>
        - name: {{ $vol.name }}
          {{- if hasKey $vol "configMap" }}
          configMap:
            name: {{ $vol.configMap }}
          {{- end }}
          {{- if and $vol.isEmptyDir }}
          emptyDir: {}
          {{- end }}
        <b>{{- end }}</b>
</pre>

We have also used <b>hasKey</b> function to determine if a specific key is defined in .Values file.

In order to run this, we need to addapt the .Values file to allow the definition of a list of volumes. The volume section at the end of the file should be changed like this:
<pre>
............

#---------Volumes values---------#
volumes:
  - name: application-external-config
    mountPath: /usr/bin/bookinfo-reviews/config/
    subPath: application.properties
    configMap: configmap-bookinfo-reviews

</pre>

If you run now, the result should be the same as before, because we still have only one volume defined, for the configMap.

Let's now add a new volume in the .Values file:
<pre>
............

#---------Volumes values---------#
volumes:
  - name: application-external-config
    mountPath: /usr/bin/bookinfo-reviews/config/
    subPath: application.properties
    configMap: configmap-bookinfo-reviews
  <b>- name: application-empty-dir
    mountPath: /usr/bin/bookinfo-reviews/emptyDir/
    isEmptyDir: true</b> 

</pre>

If you run again, you should see now two volumes in the result, like this:
<pre class="file">
apiVersion: apps/v1
kind: Deployment
metadata:
  name: bookinfo-reviews
  namespace: default
  labels:
    app: bookinfo
    tier: backend
    version: RELEASE-NAME-1
spec:
  replicas: 1
  selector:
    matchLabels:
      service: reviews
  template:
    metadata:
      labels:
        service: reviews
    spec:
      containers:
        - name: bookinfo-reviews
          image: cloudacademyibmro/bookinfo-reviews:1.0
          env:
            - name: DB_PORT
              value: "5432"
            - name: DB_HOSTNAME
              value: bookinfo-reviews-db
            - name: PORT
              value: "8080"
          volumeMounts:
            - mountPath: /usr/bin/bookinfo-reviews/config/
              name: application-external-config
              subPath: application.properties
            - mountPath: /usr/bin/bookinfo-reviews/emptyDir/
              name: application-empty-dir

      volumes:
        - name: application-external-config
          configMap:
            name: configmap-bookinfo-reviews
        - name: application-empty-dir
          emptyDir: {}
</pre>