<hr>

Let's containerize the `booinfo-details`. This is also a `node` server application but it does use a `Mongo` database.  The Dockerfile will be very similar to the one we used for `bookinfo-recommendations`. You can try this one on your own and then check if you got it right.

<pre>
FROM node:14-slim
WORKDIR '/usr/src/bookinfo-details'
COPY package*.json ./
RUN npm install
COPY . .
CMD ["npm", "run", "start"]
</pre>

Let's build the image. Don't forget that you need to be in the `bookinfo-details` directory, where the Dockerfile is. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker build -t bookinfo-details:1.0 .
</span>

`bookinfo-details` needs a `MongoDB` instance to store the details about the books. Let's run a `mongo` attached to the `bookinfo-network`

Create a volume so mongo can persist the data. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker volume create mongo-volume
</span>

And run the container. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d --name my-mongo --mount source=mongo-volume,target=/etc/mongo --network bookinfo-network mongo
</span>


And now let's run it. We will attach the container to the network `bookinfo-network`.
We also need to specify the database url. We will use the `mongo` container's name as the hostname for the database url.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -p 3003:3000 --name bookinfo-details -e DATABASE_URL=mongodb://my-mongo:27017 --network bookinfo-network  bookinfo-details:1.0
</span>

Test the application by creating a new book using:

`curl -X POST -d {'"name":"Book1", "author": "author1", "publisher":"publi1", "ISBN":"12345"'} -H 'Content-Type: application/json' http://localhost:3003/details-app/books`{{execute}}

Retrieve the list of books:

`curl http://localhost:3003/details-app/books`