#!/bin/bash
echo "Waiting to initialize with previous step progress..."
#restoring repo state
git clone --branch kubernetes-training-step5 https://gitlab.com/cloudacademy1/bookinfo-practice.git

#restoring working directory
cd bookinfo-practice/

#restore docker images locally
docker images | grep "bookinfo-recommendations:2.0" &> /dev/null
if [[ $? != 0 ]]; then
    docker build -t bookinfo-recommendations:2.0 bookinfo-recommendations
fi

#restore minikube
minikube status | grep "host: Running\nkubelet: Running\napiserver: Running\nkubeconfig: Configured" &> /dev/null
if [[ $? != 0 ]]; then
   minikube start
fi

#restore cluster state
kubectl apply -f bookinfo-kubernetes

echo "Done"
