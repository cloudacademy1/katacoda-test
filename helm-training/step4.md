Now that we briefly learned how helm charts work we can try 
creating our own.

Navigate to _**bookinfo-reviews**_ folder.

To create a chart skeleton we will use the following command:

`helm create <chart_name>`

Let's create our chart:

<pre class="file" data-target="clipboard">
helm create bookinfo-reviews-chart
</pre>

A new folder inside bookinfo-reviews has been created: _**bookinfo-reviews-chart**_.
If you explore the folder you will observe the following structure:

```shell
bookinfo-reviews-chart
├── charts
├── Chart.yaml
├── templates
│   ├── deployment.yaml
│   ├── _helpers.tpl
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── NOTES.txt
│   ├── serviceaccount.yaml
│   ├── service.yaml
│   └── tests
│       └── test-connection.yaml
└── values.yaml
```

`helm create` command creates skeleton files for a basic chart.

Here is a brief description for the main files in this folder structure:

```shell
bookinfo-reviews-chart/
  Chart.yaml          # A YAML file containing information about the chart
  values.yaml         # The default configuration values for this chart
  charts/             # A directory containing any charts upon which this chart depends.
  templates/          # A directory of templates that, when combined with values,
  templates/NOTES.txt # OPTIONAL: A plain text file containing short usage notes
```

We can delete the _**charts**_ folder as there are no chart
dependencies for this project:
<pre class="file" data-target="clipboard">
rmdir charts
</pre>

Looking in the _**templates**_ directory we can observe some
files that were not used in the chart for `bookinfo-ratings`:

```shell
hpa.yaml  # Horizontal pod-autoscaler. Configure pod scalling strategy.
ingress.yaml # An API object that manages external access to the services in a cluster, typically HTTP.
             # Ingress may provide load balancing, SSL termination and name-based virtual hosting.
serviceaccount.yaml # A service account provides an identity for processes that run in a Pod.
                    # If not specified pods will be assigned default service account.
                    # Pods can use k8s API thus they need a system user for calling the API.
```

We will not use these files for our chart. You can delete them.
Navigate to templates folder and execute:

<pre class="file" data-target="clipboard">
ls | grep -E "(hpa)|(ingress)|(account)" | xargs rm
</pre>


Now with a clean chart we can start configuring it for bookinfo-reviews. 

