Until now we ran containers using 3 images, but where do those images came from? In the database example we used the `postgres` image, but what if we wanted to use a specific image of `postgres` or want to store somehow multiple versions of our application? 

The answer to all those questions is the container **`registry`**. A `registry` is basically a repository of container's images. 


[Docker Hub](https://hub.docker.com/) is Docker's public and free to use registry that we can use to store and retrieve our own images but also some of the most well known and used applications packaged as images.  All images used so far came from this registry, which is default registry that Docker goes too if no other registry is configured. 

The registry is just a server application, so we can create private registries where we can store images. We can even run an image registry using a container.

When we get an image from a registry we say that we `pull an image from the registry` and when storing an image we say we `push the image to the registry`.

To pull an image from Docker Hub doesn't require an account, but to store your own images you must have one. You can go ahead an create one right now because we are going to need from this point. Remember your Docker ID. [Sign up here!](https://hub.docker.com/signup)

After you created the account you can connect your docker instance to the registry by using:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker login
</span>

You will be prompted for your credentials.

The image that we used in the second step, `cloudacademyibmro/message-app-demo:1.0`, is a custom image created for this exercise and store on Docker Hub. It is stored under the docker id "cloudacademyibmro" in order to avoid clashes with other images. 

The image name is only `message-app-demo`, but we also specified `:1.0` after it's name. The `:1.0` is the **image tag**. It is ussually used to uniquely identify a image of an application. It doesn't have to be a number, it can be anything, usually each organization has each own tagging strategy. If an image has no tag, it will be automatically tagger by Docker with **`latest`**. 

Be careful when pushing an image because if there is already an existing image with the same tag it will get overwritten. When you are pulling an image or extending an image **never user the tag `latest`**. If a new version of that image with the tag `latest` will be pushed to the registry, Docker will use the newer image and you don't want to update your image right away. 

In this case we didn't need to specify the domain. By default the docker daemon will go the `docker.io` which is the domain for the Docker Hub registry but if you are using a different registry you must also specify it. 

The complete name of the image should have the form: **`my-registry:port/account-id/app-name:my-tag`**. In the case of our image, if we wanted to specify it's whole name we would use `docker.io/cloudacademyibmro/message-app-demo:1.0`. 


When you tell Docker to run a container, it will first look up the image locally, if it cannot find it they goes to the registry and searches for it. If you just want to pull an image from the registry, but not run a container of it, you can do it with `pull`.

<hr>
To see all of this in action let's try to push the image `message-app-demo` to Docker Hub under your id. 

First make sure you have the image locally. To see all the image you have locally you can use:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker images
</span>

Let's delete this image and pull it again to see the `pull` command at work.

To remove the local image:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker rmi cloudacademyibmro/message-app-demo:1.0
</span>

To pull the image again:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker pull cloudacademyibmro/message-app-demo:1.0
</span>

In order for Docker to know where to push the image we have to change it's name, to be under your id. To do that we will use the `tag` command. The first argument is the source image name and the target is the new source image name. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker tag cloudacademyibmro/message-app-demo:1.0 <i>your_docker_id</i>/message-app-demo:1.0
</span>

If you run `docker images` again you should see the new image. Docker does not actually duplicates the image to do this, it just keeps two references to the same image. 

Let's push the image:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker push <i>your_docker_id</i>/message-app-demo:1.0
</span>

<hr>

Now let's try to run a private registry locally. To do that we will use the  image `registry`, also available on Docker Hub

To start the registry:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -p 5000:5000 --name my-registry registry:2
</span>

Let's tag the `message-app-demo` for the new registry. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker tag cloudacademyibmro/message-app-demo:1.0 localhost:5000/message-app-demo:1.0
</span>

Let's push the image to the private registry. 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker push localhost:5000/message-app-demo:1.0
</span>

Let's delete it from local and pull it again, but this time from the private 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker rmi localhost:5000/message-app-demo:1.0
</span>

And let's pull it again, but this time from the private registry

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker pull localhost:5000/message-app-demo:1.0
</span>

You noticed that we didn't specify any account id in our private registry. That's because we don't need to specify one. The account id can actually be a longer path where docker can find the image. In  the example where we pushed to Docker Hub you could only push images that are under the path that starts with your Docker ID. 

But the private registry that we used does not have any restricitions so we can specify any path we want, even if it doesn't exist. We can something like  **localhost:5000/foo/bar/message-app-demo:1.0**

Stop your registry and remove the data

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker container stop my-registry && docker container rm -v my-registry
</span>

<hr>

When we pulled the `postgres` and `hello-world` images we just specified the image name, no domain, no path to the image. That's because Docker looks first under `docker.io/library` where the most known and used open source images are. So not having to specify the full path is just a shortcut that Docker offers for very used images. 

Next we will start talking about how we can build our own images by extending other images.









