Let's see how we can run a Postgres database using containers: 

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run -d -e POSTGRES_PASSWORD=password --name my-database postgres
</span>

We told docker to start a new detached container named `my-database` using the `postgres` image. We also passed the environment variable `POSTGRES_PASSWORD` since this does not have a default value and is required. More info about the predefined configuration you can find [here](https://hub.docker.com/_/postgres).

To check if docker started the container run:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker ps
</span>

If everything is ok, now we could open a terminal inside the container and try to connect to the database.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker exec -it my-database /bin/bash
</span>

To connect to the database execute(-h host, -p port, -U username):

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    psql -h localhost -p 5432 -U postgres
</span>

Once you are connected to the database you can execute the following sql commands to create a simple table. 

<pre class="file" data-target="clipboard">
create schema test;
</pre>
<pre class="file" data-target="clipboard">
create table test.user(id int, name varchar(20));
</pre>
<pre class="file" data-target="clipboard">
insert into test.user(id, name) values (1, 'John'); 
</pre>
<pre class="file" data-target="clipboard">
select * from test.user;
</pre>

You can run `\q` to return to the container terminal and again `exit` to return to your terminal.

<hr>

Now let's try to connect to the database from outside the container. 
In order to do that we need to install a postgres client. If you are doing this exercise on katacoda you don't have `psql` installed on this environment. To install just run the following command. 

`git clone https://gitlab.com/cloudacademy1/postgres-binaries \
&& cd postgres-binaries \
&& tar -xf postgresql-10.16-1-linux-x64-binaries.tar.gz \
&& export PATH=$(pwd)/pgsql/bin:$PATH`{{execute}}


Now let's try to connect to the database.

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    psql -h localhost -p 5432 -U postgres
</span>

The error message says that postgres is not running on port 5432 like it expects. Why is that? 
If you remember from the previous step, we said that a container has it's own separate virtual network that is separated from the host network, we can't access it from localhost, it's like trying to access a remote server. 

In the previous example, when we use `localhost` as the hostname from inside the container it worked because the container has it's own loopback network interface.

In order to be able to connect from the host, we can use docker to do a port forwarding of all traffic on a port of the host machine  to the docker virtual network. 

In order to do this we need to add some extra flags when we start the container. 
To do that we need to remove our existing container and rerun it with the extra flags.


<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    1. docker stop my-database && docker rm my-database
</div>
<div style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    2. docker run -d -e POSTGRES_PASSWORD=password --name my-database <b>-p 5432:5432</b> postgres
</div>

When specifying **-p 5432:5432** we say that we want all trafic that goes through the host network on port 5432 to be redirected to the container virtual network on port 5432. So the first port reference the host machine's port and the second port is the container's port.

You can try again to connect to the database from the host using psql with the same command as before:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    psql -h localhost -p 5432 -U postgres
</span>


To see how docker does this behind the scenes let's use `netstat`:

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    netstat -anep | grep 5432
</span>


You should see a process called docker-proxy listening on port 5432 of the host machine. This is the process that catches all trafic to port 5432 of the host machine and forwards the traffic to the container. If you want a more in-depth explanation on how this works you can check this articles: [1](https://docs.docker.com/network/), [2](https://windsock.io/the-docker-proxy/).

<hr>
Another important characteristic of containers is that they should be stateless and ephemeral. This is in contradiction with what a database should be. In the next chapter we will see how we can overcome this problem using `volumes`.