Start the minikube cluster. For this exercise we will not need to deploy the application using the yaml files created in the Kubernetes exercise, we will be using only Helm. 
<pre class="file" data-target="clipboard">
minikube start
kubectl get namespaces
</pre>

Let's see Helm in action by Postgres with a Helm chart instead of our deployment. 

1. Install  helm:

This script will get you the latest version of Helm. 
<pre class="file" data-target="clipboard">
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
chmod 700 get_helm.sh && \
./get_helm.sh
</pre>

If you are on Katacoda you will already have `helm` installed, but it's an older version that will not work with our examples. Install this new version and override the  existing `helm` using: 
<pre class="file" data-target="clipboard">
alias helm=/usr/local/bin/helm
</pre>

2. Add a repository
   
To add a repository we execute
<pre>
    helm repo add <i><u>name</u> <u>url</u></i>
</pre>

Let's add the repository provided by the Helm team.
<pre class="file" data-target="clipboard">
    helm repo add bitnami https://charts.bitnami.com/bitnami
</pre>

To see the list of repositories available you can use:
<pre class="file" data-target="clipboard">
    helm repo ls
</pre>

You should see the new added repository.
<pre>
    NAME   	URL                                       
    <b>bitnami 	https://charts.bitnami.com/bitnami</b>
</pre>

Let's search for an image. 

<pre class="file">
    helm search repo <i><u>name_to_search_for</u></i>
</pre>
<pre class="file" data-target="clipboard">
    helm search repo postgres
</pre>

This command will search in all of the repositories we have added. 

1. Install `Postgres` using helm

The command to install a chart is 
<pre>
helm install <i><u>release-name</u> <u>chart-name-to-install</u></i>
</pre>

Remember that we said that a `chart` can be parameterized. In order to set a parameter we can add to the above command `--set propertyName=value`. 
Let's set the database name, user and password, maximum size of the database but also the name of the service because we will use this as a hostname.

Let's install our `Postgres` instance. 
<pre>
helm install <b>bookinfo-ratings-db bitnami/postgresql</b> \
--set global.postgresql.postgresqlDatabase=ratingsdb \
--set global.postgresql.postgresqlUsername=postgres \
--set global.postgresql.postgresqlPassword=password \
--set persistence.size=512Mi \
--set fullnameOverride=bookinfo-ratings-db
</pre>

You can find a full list of parameters for this chart [here](https://github.com/bitnami/charts/tree/master/bitnami/postgresql) or using the command `helm show readme bitnami/postgresql`

After you execute the install command you should see a summary of the release. You can get the same message using `kubectl status name_of_release`.
<pre class="file">
NAME: bookinfo-ratings-db 
LAST DEPLOYED: Mon Mar 15 15:27:01 2021
NAMESPACE: bookinfo-ns
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
    ....
</pre>

Let's how the cluster looks now, after the installation. 
<pre>
kubectl get pv
kubectl get pvc
kubectl get all
</pre>

We see here 1 statefulset object, 1 pod, 2 services, 1 PV and 1 PVC. A stateful set is similar to a deployment with a few differences, used in general for stateful applications, in our case a database, read more about it [here](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)). Also one of the services is called a headless service, but we are not going to use it, you can read more about it [here](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services) and [here](https://stackoverflow.com/questions/52707840/what-exactly-is-a-headless-service-what-does-it-do-accomplish-and-what-are-som)

You can use `kubectl describe` if you want  to see more details about each object. 

Let's see the list of releases currently installed in our cluster. For that we use:
<pre>
    helm ls
    NAME               	NAMESPACE	REVISION	UPDATED                               	STATUS  	CHART            	APP VERSION
bookinfo-ratings-db	default  	1       	2021-03-15 15:45:38.12309366 +0200 EET	deployed	postgresql-8.10.5	11.8.0 
</pre>

We see that we are at `revision 1` of this release. Let's change the release by modifying the username and password that we set. For that we will use `upgrade` command instead of `install`. 
Before doing the upgrade check the current value for `POSTGRES_USER`: 
<pre class="file" data-target="clipboard">
kubectl describe pod/bookinfo-ratings-db-0 | grep POSTGRES_USER
</pre>

<pre class="file" data-target="clipboard">
helm <b>upgrade</b> bookinfo-ratings-db bitnami/postgresql \
--set global.postgresql.postgresqlDatabase=ratingsdb \
--set global.postgresql.postgresqlUsername=<b>anotherPostgresUser</b> \
--set global.postgresql.postgresqlPassword=password \
--set persistence.size=512Mi \
--set fullnameOverride=bookinfo-ratings-db
</pre>

Let's test the change with:
<pre class="file" data-target="clipboard">
kubectl describe pod/bookinfo-ratings-db-0 | grep POSTGRES_USER
</pre>

Let's take a look now at our release with `helm ls`. You should see that the revision was incremented.

It is even easier to do a rollback:
<pre class="file" data-target="clipboard">
helm rollback <i><u>release-name</u> [<u>target-revision</u>]</i>
</pre>

If we don't specify the `target-revision`, helm will do a rollback to the previous revision.

Let's test it on out cluster.
<pre class="file" data-target="clipboard">
helm rollback bookinfo-ratings-db 1</i>
</pre>

Check again with:
<pre class="file" data-target="clipboard">
kubectl describe pod/bookinfo-ratings-db-0 | grep POSTGRES_USER
</pre>

If we take a look at the list of releases we will see that now we are at revision 3, the revision did't change back to one, but it got incremented again. 

To see more information run `helm history release-name`. You should see in the description of revision 3 that is actually a rollback to revision 1. 
<pre class="file" data-target="clipboard">
helm history bookinfo-ratings-db
REVISION	UPDATED                 	STATUS    	CHART            	APP VERSION	DESCRIPTION     
1       	Mon Mar 15 16:02:22 2021	superseded	postgresql-8.10.5	11.8.0     	Install complete
2       	Mon Mar 15 16:05:10 2021	superseded	postgresql-8.10.5	11.8.0     	Upgrade complete
3       	Mon Mar 15 16:13:43 2021	deployed  	postgresql-8.10.5	11.8.0     	<b>Rollback to 1</b>   

</pre>

There is an option to rollback to the latest succesful revision, just set the target revision to 0: 
<pre class="file" data-target="clipboard">
helm rollback <i><u>release-name</u></i> 0
</pre>

To uninstall a release we can simply use the `uninstall` command of helm. 

<pre class="file" data-target="clipboard">
helm uninstall bookinfo-ratings-db
</pre>

Let's install again the chart, but this time we will pass the parameters to helm using of file instead of enumerating them in the command line. 
First create a file called `bookinfo-ratings-db-chart-values.yaml` and add the values here:
<pre class="file" data-target="clipboard">
global:
  postgresql:
    postgresqlDatabase: ratingsdb
    postgresqlUsername: postgres
    postgresqlPassword: password

persistence:
  size: 521Mi

fullnameOverride: bookinfo-ratings-db
</pre>

Install the chart by passing the file with the `--values filename` option
<pre class="file" data-target="clipboard">
helm install bookinfo-ratings-db  bitnami/postgresql --values bookinfo-ratings-db-chart-values.yaml
</pre>

Next we will see a custom chart created for the `bookinfo-ratings` application and then you will create your own chart for the `bookinfo-reviews` application.