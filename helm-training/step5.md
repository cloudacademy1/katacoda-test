Before starting chart configuration let's recap the 
main configuration files:

```shell
Chart.yaml
values.yaml
```

Besides these 2 files there is also a Helm built-in logical object
called `Release`.

`Chart.yaml` file provides a summary of the chart.
It contains information like the chart name, description, version, application version, etc.

`values.yaml` contain variables definitions and values that will be 
used in k8s resources located in templates folder. Variables placeholders
from k8s resources will be replaced with the values from this file.

`Release` is one of the top-level objects that you can access in your templates.
It contains information about your installed release in the k8s cluster:

* Release: This object describes the release itself. It has several objects inside it:
    * Release.Name: The release name
    * Release.Namespace: The namespace to be released into (if the manifest doesn’t override)
    * Release.IsUpgrade: This is set to true if the current operation is an upgrade or rollback.
    * Release.IsInstall: This is set to true if the current operation is an install.
    * Release.Revision: The revision number for this release. On install, this is 1, and it is incremented with each upgrade and rollback.
    * Release.Service: The service that is rendering the present template. On Helm, this is always Helm.

Open `Chart.yaml` the file.
We can observe the following fields:
```yaml
# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
appVersion: 1.16.0
```

Let's change both versions to `1.0.0` as this is the version of our
chart and application.

We should also modify the `templates/NOTES.txt` file.
Clear its content and use a more specific description:
<pre class="file" data-target="clipboard">
Thank you for installing {{ .Chart.Name }}.

Your release is named {{ .Release.Name }}.

To learn more about the release, try:

  $ helm status {{ .Release.Name }}
  $ helm get all {{ .Release.Name }}
</pre>

Looking into `values.yaml` we can observe a lot of configurations.
Given the fact that our application is pretty simple we can delete these
configurations and use simpler ones. 

Delete the content of values.yaml.
In the next exercise we will configure the values for our k8s resources.

