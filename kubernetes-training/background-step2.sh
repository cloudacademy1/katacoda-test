#!/bin/bash

echo "Waiting to initialize with previous step progress..."
git clone --branch kubernetes-training https://gitlab.com/cloudacademy1/bookinfo-practice.git
cd bookinfo-practice/
docker build -t bookinfo-recommendations:1.0 bookinfo-recommendations
cd ..
minikube start
echo "Done" >> /opt/.background-step2-finished
