
Our applications depend on other libraries, configuration files, or services that
are provided by the runtime environment.


The traditional runtime environment for a software
application is a physical host or virtual machine, and application dependencies are installed as part
of the host.

Some of the drawbacks to this approach are: 
* difficult to update shared libraries used by multiple applications, even if the update does not break the other application it will cause some down time
* environments duplication can be hard 
* if you have multiple environments, in time, you start having differences between these environments
* difficult to make updates to the host operating system, sometimes critical security updates, because you risk to break the application

With the container approach we will package each application together with all the dependencies that it requires. We  will call this package an **image**. 

Using a **container runtime** and the **image** as blueprints we can create **containers**. 

For now you can think of a container as a virtual machine with a very fast boot time. We will see later exactly what a container really is. 

In the animation below you can see two apps(APP1 and APP2). 
APP1 requires a Java Runtime and an Oracle lib. APP2 requires the same Oracle lib as APP1. 

We could create 2 images, one for APP1 with the application, the JRE and the Oracle lib, and another one for APP2 with the application and with it's own Oracle lib. We can then create containers from each image where the applications can run 100% independently. 

![Containers vs Traditional systems](https://s2.gifyu.com/images/ezgif.com-gif-maker-32588eeec0264acde.gif)

If we want to create a new environment for one of the two apps we  can just reuse our image(the blueprint) to create another container that will run independently from the first, even on the same machine. 


Another advantage to the containers approach is that you can reuse an existing image by extending it. For example you can create an image that includes all the common dependencies that all the Java applications in your organization use(application server, common configurations, etc). For each application you can start from the common image and just add the application and specific dependencies. 

Also with containers you can simplify the installation of third party applications. For example a database provider can create an image with the database binaries and all it's dependencies. You can extend that image just with your specific configuration files and then just run a container using the resulting image. You want to install a new instance of the database, just start another container.


**Docker** provides a container runtime. But Docker is not just that, it also provides us a way to **build the images**, **interact with registry**(repository for images), **interact with containers**, **manage images** that are stored locally and other things.

Docker consists of a **client** and a **daemon**. The daemon does all the heavy lifting but we don't interact with it directly, for that we will use the client.


![Docker arhitecture](https://docs.docker.com/engine/images/architecture.svg)
Source: [Docker](https://docs.docker.com/get-started/overview)

Let's see it in action. We will run a very simple container from an image called __hello-world__

<span style="background-color: #555; padding: 3px 7px 3px 7px; color: white;">
    docker run hello-world 
</span>

You should get an explanation on how the container got started and the hello world message.

Each container starts with a main process. That process is defined in the image. Once that process finishes the execution the container also exits, killing all other process that might run inside the container. 

In the above example, the main process was just a small script that wrote to the output stream the explanation and the message. Once the script exited, the container also stopped, returning execution control to the terminal process on the host machine.

In the next step we will talk about some common docker commands and we will run some more containers and then even install a Postgres instance using containers. 