IMPORTANT: If you did not fully complete previous steps or if your session expired and you want to resume your learning starting from this step, please run the script below. Otherwise please skip this part.
<pre class="file" data-target="clipboard">
wget https://gitlab.com/-/snippets/2085708/raw/master/restore-k8s.sh && chmod +x restore-k8s.sh && source restore-k8s.sh kubernetes-training-step5
</pre>

**1. Create a ConfigMap**  
A `ConfigMap` is a Kubernetes object that can store data in key-value pairs. We can use it to pass environment variables to our pods. 
A `ConfigMap` will help in decoupling the application from the environment even more. For example, we can have the same deployment for local and cloud environment and keep all the environment specific information in a ConfigMap. 

Let's create one in which we will store the `LIBRARIAN_NAME` variable for our `book-recommendations`. Create a file called `book-recommendations-configmap.yaml`
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: ConfigMap
metadata:
  name: book-recommendations-configmap
data:
  LIBRARIAN_NAME: "Ion" 
 </pre>
The `name` in the metadata object will be used to reference this configmap.  
The `data` object is where we can define our key-value pairs.  

Let's configure the deployment to use this ConfigMap. 
<pre class="file" data-target="clipboard">
...
spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        env:
          - name: LIBRARIAN_NAME
            <strike>value: John</strike>
            <b>valueFrom:
              configMapKeyRef:
                name: book-recommendations-configmap
                key: LIBRARIAN_NAME</b>
...
</pre> 

Let's apply the new configuration
<pre>
$ kubectl apply -f bookinfo-kubernetes/
<b>configmap/book-recommendations-configmap created
deployment.apps/bookinfo-recommendations configured</b>
service/bookinfo-recommendations unchanged
deployment.apps/bookinfo-ui unchanged
service/bookinfo-ui unchanged
</pre>

Our configmap was created and `bookinfo-recommendations` deployment was updated.
We can also see our new pods
<pre>
$ kubectl get pods
NAME                                      READY   STATUS    RESTARTS   AGE
<b>bookinfo-recommendations-b558bbd9-rmrt5   1/1     Running   0          3m46s
bookinfo-recommendations-b558bbd9-x79vx   1/1     Running   0          3m41s</b>
</pre>

Let's take a look at the logs, see if the environment variable changed. 
<pre>
$ kubectl logs bookinfo-recommendations-b558bbd9-rmrt5

> bookinfo-recommendations@1.0.0 start /usr/src/bookinfo-recommendations
> node index.js

<b>Hello! My name is Ion</b>
</pre>

In this example we specified a key from our configmap, but we can pass all the variables at once to our pod. For this we have to change again our deployment config.

<pre class="file" data-target="clipboard">
spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        <strike>env:
          - name: LIBRARIAN_NAME
            valueFrom:
              configMapKeyRef:
                name: book-recommendations-configmap
                key: LIBRARIAN_NAME</strike>
        <b>envFrom:
          - configMapRef:
              name: book-recommendations-configmap</b>
</pre>

```
kubectl apply -f bookinfo-kubernetes
kubectl get pods
kubectl logs bookinfo-recommendations-5df6bd96c7-966fd
```

We can also use a ConfigMap to hold configuration files and then we can mount them to our pods.   
For this example we will use a configmap to configure the nginx server that is serving our angular app. 

Let's take a look at the Dockerfile for our `bookinfo-ui`
<pre>
FROM node:14-slim as builder 
WORKDIR '/usr/src/bookinfo'
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

<b>FROM nginx:1.17.10-alpine as prod-without-config</b>
COPY --from=builder /usr/src/bookinfo/dist/bookinfo-ui /usr/share/nginx/html

FROM prod-without-config as prod
<b>COPY nginx.conf /etc/nginx/conf.d/default.conf</b>
</pre>

The final stage called `prod` is already copying the configuration files. This means that if we change something in the nginx configuration we will have to rebuild and push the image. 
What we want to use is the `prod-without-config` stage which contains just the angular application. 

*If you are on Katacoda, skip the image build and use the images already built from cloudacademyibmro repository.
Let's build it by specifying the target stage and push this image. Don't forget to change the username.
<pre>
docker build -t bookinfo-ui:<b>2.0</b> <b>--target prod-without-config</b> bookinfo-ui
</pre>

We also have to change the `bookinfo-ui-deployment.yaml` to use the new image. 
<pre>
...
image: cloudacademyibmro/bookinfo-ui:<b>2.0</b>
...
</pre>

Let's apply this new configuration and test the application
```
kubectl apply -f bookinfo-kubernetes
minikube service bookinfo-ui --url
```
For Katacoda you won't be able to access the application using the url from minikube. You have to take the port and replace it in this url: https://[[HOST_SUBDOMAIN]]-PORT_HERE-[[KATACODA_HOST]].environments.katacoda.com/

The application no longer works because it no longer has the nginx configuration file. 
Let's fix this with a configmap. Create a file called `bookinfo-ui-configmap.yaml`.
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: ConfigMap
metadata:
  name: book-ui-configmap
data:
  nginx.conf: |
    
    upstream bookinfo-recommendations {
        server bookinfo-recommendations:3000;
    }

    server {
        listen       80;
        server_name  localhost;
        resolver kube-dns.kube-system.svc.cluster.local. valid=30s;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }

        location /recommendations-app {
            proxy_pass http://bookinfo-recommendations;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }
    }
</pre>

We pass the file content as a value to the `nginx.con` key by using the `|` symbol. This symbol let's you write a multiline string in a yaml file. 
The key is also important here, Kubernetes will pass this value as a file and the file name will be the key. 

Let's add this config to our deployment. 
We will add  a `volumes` entry to our spec, define one volume and then reference that volume from our container definition. 
<pre class="file" data-target="clipboard">
spec:
      <b>volumes:
        - name: nginx-config
          configMap: 
            name: book-ui-configmap</b>
      containers:
      - name: bookinfo-ui
        image: cloudacademyibmro/bookinfo-ui:2.0
        <b>volumeMounts:
          - name: nginx-config
            mountPath: /etc/nginx/conf.d/</b>
</pre>
We added a `volumes` entry to our spec object. We specify a volume called `nginx-config` and this volume will actually be a file coming from our configmap. 
We also have to reference that volume in our container(`volumeMounts`) and the mounting point(`mountPath`), where Kubernetes will put the file coming from the configmap. 

Let's reapply this configuration and test the application again
<pre>
kubectl apply -f bookinfo-kubernetes/
# make sure the new pod started
kubectl get pods
# you can test the application on localhost:4200
</pre>

We can see the configmaps in the cluster with this command:
```
kubectl get configmaps
```

You can read more about `ConfigMap` [here](https://kubernetes.io/docs/concepts/configuration/configmap/) and [here](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/).

**2. Create a Secret**  
Secret are similar to ConfigMaps, we can use it to hold data as key-value pairs but they are handled more careful by Kubernetes than ConfigMaps. They are not saved on disk on none of the working nodes and are only kept in the `etcd` database on the master.   
We should put in a secret configuration that is sensitive, like a database password or an api key and access to those objects should be less permissive. Also, we should not be keeping the secret configuration files in the project. 

Let's try to pass our `LIBRARIAN_NAME` as a secret to `bookinfo-recommendations`.
For our example we will keep the configuration in the project since is not really a security concern.  Let's create a `bookinfo-recommendations-secret.yaml`.
<pre class="file" data-target="clipboard">
apiVersion: v1
kind: Secret
metadata:
  name: librarian-secret
type: Opaque
data:
  LIBRARIAN_NAME: Sm9obgo=
</pre>

We need to specify the type of Secret. Kubernetes has support for special types of secrets, like TLS certificates, private registry credentials, Basic Auth, SSH Auth. For this example we will be using 'Opaque' type which is a generic type where we can define keys ourselves.
The key is in clear text but the value needs to be encoded in base64 (encoding does not provide any security, it's the same as clear text from a security point of view).

You can encode a string using the `base64` utility in `bash`  
Ex: `echo John | base64`  
Output: `Sm9obgo=`  

We could also create a secret using only the kubectl:
```
kubectl create secret generic librarian-secret --from-literal=LIBRARIAN_NAME=John
```

Notice that we don't have to  encode the value when we use this method. 
We used `--from-literal` to pass the values on the command line, but you should use `--from-file` to pass multiple values from a file since passwords on the command line are not a good idea. 

Let's modify the deployment to use the value from the secret (`bookinfo-recommendations-deployment.ymal`). As with configmaps, we can specify the exact name of the key that we want to use or just pass the whole secret. You can pick one of them
<pre class="file" data-target="clipboard">
spec:
  containers:
    - name: bookinfo-recommendations
      image: cloudacademyibmro/bookinfo-recommendations:2.0
      <b>env:
        - name: LIBRARIAN_NAME
          valueFrom:
            secretKeyRef:
              name: librarian-secret
              key: LIBRARIAN_NAME</b>
</pre>
or
<pre class="file" data-target="clipboard">
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        <b>envFrom:
          - secretRef:
              name: librarian-secret</b>
</pre>

Let's test it: 

<pre>
$ kubectl apply -f bookinfo-kubernetes
$ kubectl get pods
$ kubectl logs bookinfo-recommendations-749d5b4fdf-qwwxf

> bookinfo-recommendations@1.0.0 start /usr/src/bookinfo-recommendations
> node index.js

<b>Hello! My name is John</b>
</pre>

**3. Create a secret to pull images from a private registry**  
Let's use a special kind of secret in Kubernetes. We will use this secret to pull images from a registry. The registry from which we've pulling images until now is public but we can still use a secret to access  it. 

We can use the kubectl. We must specify the secret type: `docker-registry` and the information about the registry: 
<pre>
kubectl create secret docker-registry my-registry-secret --docker-server=https://index.docker.io/v1/ --docker-username=<i>your_username</i> --docker-password=<i>your_password</i>
</pre>
Notice the `docker-registry` option here. That is the secret type, a special Secret type built-in into Kubernetes.
`https://index.docker.io/v1/ ` is the server name for DockerHub. 

<pre>
kubectl get secrets
</pre>
You can see a secret called `my-registry-secret` of type `kubernetes.io/dockerconfigjson`

We can use this secret to pull images by specifying it in the deployment template spec as a `imagePullSecrets`. Notice that this property is at the same level with `containers` so all containers will be pulled using this secret.   
`bookinfo-recommendations-deployment.yaml`
<pre class="file" data-target="clipboard">
...
    spec:
      containers:
      - name: bookinfo-recommendations
        image: cloudacademyibmro/bookinfo-recommendations:2.0
        envFrom:
          - secretRef:
                name: librarian-secret
      <b>imagePullSecrets: 
        - name: my-registry-secret</b>
...
</pre>

<pre>
kubectl apply -f bookinfo-kubernetes
</pre>

You can read more about Secrets [here](https://kubernetes.io/docs/concepts/configuration/secret/#)