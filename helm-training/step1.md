As you could see from the Kubernetes exercise, the number of yaml files starts to pile up. And it's harder and harder to manage them and this is just a simple application.

In production environments we will have even more files, multiple deployments, confimaps, secrets, etc as well as other types (ingress - manages the access to services from outside the cluster, network policy - control access between the services inside the cluster and outbound calls made from the cluster, etc). 

When you add in the equation that you have to deploy the application on multiple environments(different namespace, different configmaps, different secrets) this is getting even harder to manage and you start doing a lot of copy paste and it's very prone to error. 

Also, let's say you want to install a third party app, let's say a database or other middlewares, you need all the kubernetes objects in order to do it, this means having to manage it and if you want to change the default configuration(which we almost always want) we have to understand the whole installation configuration, that can be very complex.

Helm comes as a solution to those problems. With Helm we can package Kubernetes application in packages called `charts`. Those `charts` can also be parameterized in order to customize an installation. 

A `chart` that has been installed on a K8s cluster is called a `release`. 

With Helm we can install the `charts` and manage the `releases` with a few simple commands. 

Another advantage of Helm charts is that they can be reused. You can have standard charts in your organization(e.g.: a chart for Spring Boot backend applications, a chart for Angular applications). This way you enforce best practices and you have a homogenous mode of installation for all applications.

The charts can be versioned and store in a Helm `repository`.

Let's see next how to use it. 
