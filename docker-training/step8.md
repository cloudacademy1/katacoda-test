Sometimes miss-configuration can occur when building images or when trying
to run containers. How can we debug these situations?

We will see in this step.

Step 6 and 7 are prerequisites for this step. 

In the bookinfo-reviews project you should have the following script:

`bookinfo-practice/bookinfo-reviews/create_miss_configured_dockerfile.sh`

Execute this script.

It should have created a slightly miss-configured Dockerfile.

First lets package our project:

<pre class="file" data-target="clipboard">
mvn clean package
</pre>

Build the new docker image (make sure you are in the root folder of bookinfo-reviews project):

<pre class="file" data-target="clipboard">
docker build -t bookinfo-reviews:1.0 .
</pre>

Run the application as a docker container:

<pre class="file" data-target="clipboard">
docker run -it -d -p 8080:8080 --name bookinfo-reviews bookinfo-reviews:1.0
</pre>

Check the status of your container:

<pre class="file" data-target="clipboard">
docker ps
</pre>

Your container is not showing. What happened? `Docker ps` displays only running containers. To 
see all containers (stopped or running) you can use:

<pre class="file" data-target="clipboard">
docker ps -a
</pre>

---
**SIDE NOTE**

`docker` has a well written `--help` use it when searching for commands:

`docker --help | grep <keyword>`

or use it to better understand a command:

`docker <command> --help`

---

You should see an output like this:
```
CONTAINER ID  IMAGE                           COMMAND  CREATED         STATUS                     PORTS                   NAMES
b814b5d93d81  localhost/bookinfo-reviews:1.0           32 seconds ago  Exited (1) 32 seconds ago  0.0.0.0:8080->8080/tcp  bookinfo-reviews
```

So why did our container stopped abruptly? 

We can use `docker logs <container_name|container_id>` to check for more:
<pre class="file" data-target="clipboard">
docker logs bookinfo-reviews
</pre>

A similar log should appear:

```no main manifest attribute, in bookinfo-reviews.jar```

So the java process encountered a problem and it had to stop. It seems
there is a problem with our jar. How can we check our jar? Let's try to run it from the local environment.
Navigate to `bookinfo-practice/bookinfo-reviews/target` and run:
<pre class="file" data-target="clipboard">
java -jar bookinfo-reviews-0.0.1-SNAPSHOT.jar
</pre>

The application starts with no problems, so what is the problem? Maybe the jar is corrupt inside the container? First,
check the jars generated in target folder:

<pre class="file" data-target="clipboard">
ls -alhgG
</pre>

An output similar to this should appear:
```
-rw-rw-r--. 1 47M Feb 22 12:09 bookinfo-reviews-0.0.1-SNAPSHOT.jar
-rw-rw-r--. 1 11K Feb 22 12:09 bookinfo-reviews-0.0.1-SNAPSHOT.jar.original
```
How can we check the jar inside the container if the container stops instantly? We will modify the dockerfile's entrypoint
so we can run a process that does not crash the container, thus allowing us to inspect the jar.

Modify the Dockerfile to start as entrypoint a shell process. Your dockerfile should look like this:

<pre class="file" data-target="clipboard">
FROM openjdk:8-jre-alpine
WORKDIR /usr/bin/bookinfo-reviews
COPY target/*.original /usr/bin/bookinfo-reviews/bookinfo-reviews.jar
#ENTRYPOINT ["java", "-jar", "bookinfo-reviews.jar"]
ENTRYPOINT ["/bin/sh"]
</pre>

Build the image again (be sure to be in the folder where the Dockerfile is located):

<pre class="file" data-target="clipboard">
docker build -t bookinfo-reviews:1.0 .
</pre>

Run the image again. If you run `docker ps` you should see your image. Now we can explore the container and the
application jar's contents:

<pre class="file" data-target="clipboard">
docker exec -it bookinfo-reviews /bin/sh
</pre>

You should be now in the working directory of the container and where the jar is situated. Inspect the jar:
<pre class="file" data-target="clipboard">
ls -alh
</pre>

Can you observe something unusual with the jar? Compare it with the ones from the `target` folder. What is the problem?
Try to solve it without looking at the next steps. 

---
.

.

.

---

If we look closely we can see that the size of the jar is the same as the one from the target with the extension 
`.original`. This is the jar only with our source code and without any dependencies. This means that we should look
at the `COPY` command in the Dockerfile. 

Indeed, the Dockerfile copied the original:
`COPY target/*.original /usr/bin/bookinfo-reviews/bookinfo-reviews.jar`

Replace with the correct instruction and change back the entrypoint to the old one.
Build the image and run it. Now it should work correctly. 

This is the basic way to debug a docker container:
1. docker logs
2. docker ps -a
3. docker exec -it <container_name|container_id> /bin/sh

---
**SIDE NOTE**

Docker images tend to be slim and due to security reasons they integrate only the most basic cli tools in the linux file system. Don't expect
to always find tools like `telnet, wget, curl`. 

---